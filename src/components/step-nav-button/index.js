import ko from 'knockout';
import template from './step-nav-button.template.html';
import viewModel from './step-nav-button.viewmodel.js';

let synchronous = true;

ko.components.register('step-nav-button', { template, viewModel, synchronous });
