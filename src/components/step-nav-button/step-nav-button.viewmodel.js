import ko from 'knockout';
import { connect } from 'knockout-store';
import { isValid } from 'knockout.validation';

function stepNavButtonViewModel(params) {
    const vm = this;
    vm.visibleKeys = params.visibleKeys;

    vm.stepSelected = ko.pureComputed(() =>
        typeof params.selectedStep() !== 'undefined'
    );

    vm.currentStepIndex = ko.pureComputed(() =>
        params.steps().indexOf(params.selectedStep())
    );

    vm.hasPrev = ko.pureComputed(() => {
        if (vm.stepSelected()) {
            return params.selectedStep().ordinal() > params.steps()[0].ordinal();
        }
        return false;
    });
    function selectPrevStep() {

        const prevStep = params.steps()[vm.currentStepIndex() - 1];

        params.selectedStep(prevStep);

        params.rulesEngine
            .run()
            .then(({ events }) => {
                const visibleKeys = events.map(a => a.params.key);
                vm.visibleKeys(visibleKeys);
            });

        vm.scrollToTop();
    }
    vm.prevStep = selectPrevStep;

    vm.hasNext = ko.pureComputed(() => {
        if (vm.stepSelected()) {
            return params.selectedStep().ordinal() < params.steps()[params.steps().length - 1].ordinal();
        }
        return false;
    });
    function selectNextStep() {

        if (!vm.validate.isValid()) {
            vm.validate.errors.showAllMessages();

            vm.scrollToFirstInvalidControl();

            return;
        }

        params.selectedStep().done = true;

        const nextStep = params.steps()[vm.currentStepIndex() + 1];

        params.selectedStep(nextStep);

        params.rulesEngine
            .run()
            .then(({ events }) => {
                const visibleKeys = events.map(a => a.params.key);
                vm.visibleKeys(visibleKeys);
            });

        vm.validate.errors.showAllMessages(false);

        vm.scrollToTop();
    }
    vm.nextStep = selectNextStep;

    vm.hasFinish = ko.pureComputed(() => {
        if (vm.stepSelected()) {
            return params.selectedStep().ordinal() === params.steps()[params.steps().length - 1].ordinal();
        }
        return false;
    });
    function selectFinishStep() {
        params.selectedStep().done = true;
        params.selectedStep(undefined);
    }
    vm.finishStep = selectFinishStep;

    vm.validatables = ko.observableArray();

    vm.stepNavConfig = ko.computed(() => {
        if (!vm.stepSelected()) {
            return [];
        }

        const ar1 = ko.utils.arrayFilter(params.items(), (item) =>
            params.selectedStep().validatableIds().indexOf(item.id()) > -1)

        const ar = ar1.map((value) =>
            value.value);

        vm.validatables(ar);
    });

    vm.validate = ko.validatedObservable({
        items: vm.validatables
    });

    vm.scrollToFirstInvalidControl = function scrollToFirstInvalid() {
        vm.firstInvalidControl = document.querySelector('.validationMessage:not([style*="display: none;"])');
        vm.navControl = document.querySelector('step-breadcrumb');
        vm.containerControl = document.querySelector('header-component');

        window.scroll({
            top: vm.getTopOffset(vm.containerControl, vm.navControl, vm.firstInvalidControl),
            left: 0,
            behavior: "smooth"
        });
    }

    vm.scrollToTop = function scrollToTopInput() {
        vm.firstControl = document.querySelector('form-layout');
        vm.navControl = document.querySelector('step-breadcrumb');
        vm.containerControl = document.querySelector('header-component');

        window.scroll({
            top: vm.getTopOffset(vm.containerControl, vm.navControl, vm.firstControl),
            left: 0,
            behavior: "auto"
        });
    }

    vm.getTopOffset = function getTopOffset(containerEl, relativeEl, controlEl) {

        const labelOffset = 25;
        const controlElTop = controlEl.getBoundingClientRect().top;

        if (containerEl) {
            const containerTop = containerEl.getBoundingClientRect().top;
            const absoluteControlElTop = controlElTop + containerEl.scrollTop;

            return absoluteControlElTop - containerTop - labelOffset - relativeEl.offsetHeight;
        } else {
            const absoluteControlElTop = controlElTop + window.scrollY;

            return absoluteControlElTop - labelOffset;
        }
    }

    return vm;
}

stepNavButtonViewModel.prototype.dispose = function () {
    this.stepNavConfig.dispose();
}

function mapStateToParams({ rulesEngine, visibleKeys, selectedStep, items }) {
    let steps = ko.pureComputed(() => {
        return ko.utils.arrayFilter(items(), function (item) {
            return item.type === 'step';
        });
    });

    return { rulesEngine, visibleKeys, selectedStep, steps, items };
}

export default connect(mapStateToParams)(stepNavButtonViewModel);
