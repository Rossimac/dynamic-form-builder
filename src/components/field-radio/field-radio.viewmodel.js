import ko from 'knockout';
import { connect } from 'knockout-store';

function fieldRadioViewModel(params) {
    const vm = this;
    vm.id = params.id;
    vm.label = params.question.name;
    vm.value = params.question.value;
    vm.options = params.question.options;
    vm.radioOptions = params.mainOptions;

    vm.inlineOptions = ko.pureComputed(() => {
        return vm.radioOptions.length <= 2;
    });

    vm.hasOtherList = ko.pureComputed(() => {
        return vm.options.otherOptionsKey !== undefined;
    });

    vm.value.subscribe(function (newValue) {

        if (vm.hasOtherList() === false) {
            return;
        }

        const visibleValues = vm.radioOptions.map((element) => {
            return element.value;
        });

        const valueInOthers = !visibleValues.includes(newValue);

        if (valueInOthers === true) {
            vm.otherClicked(true);
            return;
        }

        vm.otherClicked(false);
    });

    function showOtherList(v) {

        if (v === undefined) {
            return false;
        }

        const visibleValues = vm.radioOptions.map((element) => {
            return element.value;
        });

        const valueInOthers = !visibleValues.includes(v);

        if (valueInOthers === true) {
            return true;
        }

        return false;
    }

    vm.otherClicked = ko.observable(showOtherList(vm.value()));

    vm.otherClicked.subscribe(function (v) {

        if (v === true) {

            const visibleValues = vm.radioOptions.map((element) => {
                return element.value;
            });

            const valueInVisibles = visibleValues.includes(vm.value());
            
            if (valueInVisibles) {
                vm.value(undefined);
                vm.value.isModified(false);
            }
        }

    });

    return vm;
}

function mapStateToParams({ optionsDefinitions, optionsLists, items }) {
    return { optionsDefinitions, optionsLists, items };
}

function mergeParams({ optionsDefinitions, optionsLists, items }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    const mainOptionsList = ko.utils.arrayFirst(optionsLists(), function (item) {
        return item.key === question.options.optionsKey;
    });

    const optionsDefinition = ko.utils.arrayFirst(optionsDefinitions(), function (item) {
        return item.key === mainOptionsList.sourceList;
    });

    const mainOptions = optionsDefinition.options.reduce((c, n) => {
        if (mainOptionsList.options.some(function (item) {
            return item === n.value;
        })) {
            c.push(n);
            return c;
        }
        return c;
    }, []);

    let otherOptions = null;

    if (question.options.otherOptionsKey !== undefined) {

        const otherOptionsList = ko.utils.arrayFirst(optionsLists(), function (item) {
            return item.key === question.options.otherOptionsKey;
        });

        otherOptions = optionsDefinition.options.reduce((c, n) => {
            if (otherOptionsList.options.some(function (item) {
                return item === n.value;
            })) {
                c.push(n);
                return c;
            }
            return c;
        }, []);
    }

    return { question, id, mainOptions, otherOptions };
}

export default connect(mapStateToParams, mergeParams)(fieldRadioViewModel);
