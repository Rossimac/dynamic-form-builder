import ko from 'knockout';
import template from './field-radio.template.html';
import viewModel from './field-radio.viewmodel.js';
import './field-radio.styles.scss';

let synchronous = true;

ko.components.register('field-radio', { template, viewModel, synchronous });
