import ko from 'knockout';
import template from './field-checkbox.template.html';
import viewModel from './field-checkbox.viewmodel.js';
import './field-checkbox.styles.scss';

let synchronous = true;

ko.components.register('field-checkbox', { template, viewModel, synchronous });
