import ko from 'knockout';
import { connect } from 'knockout-store';

function fieldRadioViewModel(params) {
    const vm = this;
    vm.value = params.question.value;
    vm.options = params.question.options;

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    return { question };
}

export default connect(mapStateToParams, mergeParams)(fieldRadioViewModel);
