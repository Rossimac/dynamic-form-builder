import ko from 'knockout';
import { connect } from 'knockout-store';

function stepBreadcrumbViewModel(params) {
    const vm = this;
    vm.visibleKeys = params.visibleKeys;

    vm.steps = params.steps;
    vm.selectedStep = params.selectedStep;

    vm.stepSelected = ko.pureComputed(() =>
        typeof params.selectedStep() !== 'undefined'
    );

    function selectStep(selectedStep) {

        if (selectedStep.step.ordinal() >= vm.selectedStep().ordinal()) {
            return;
        }

        params.selectedStep(selectedStep.step);

        params.rulesEngine
            .run()
            .then(({ events }) => {
                const visibleKeys = events.map(a => a.params.key);
                vm.visibleKeys(visibleKeys);
            });

        vm.scrollToTop();
    }
    vm.stepClicked = selectStep;

    vm.scrollToTop = function scrollToTopInput() {
        vm.firstControl = document.querySelector('form-layout');
        vm.navControl = document.querySelector('step-breadcrumb');
        vm.containerControl = document.querySelector('header-component');

        window.scroll({
            top: vm.getTopOffset(vm.containerControl, vm.navControl, vm.firstControl),
            left: 0,
            behavior: "auto"
        });
    }

    vm.scrollToFirstInvalidControl = function scrollToFirstInvalid() {
        vm.firstInvalidControl = document.querySelector('.validationMessage:not([style*="display: none;"])');
        vm.navControl = document.querySelector('step-breadcrumb');
        vm.containerControl = document.querySelector('header-component');

        window.scroll({
            top: vm.getTopOffset(vm.containerControl, vm.navControl, vm.firstInvalidControl),
            left: 0,
            behavior: "smooth"
        });
    }

    vm.validatables = ko.observableArray();

    vm.validate = ko.validatedObservable({
        items: vm.validatables
    });

    vm.stepNavConfig = ko.computed(() => {
        if (!vm.stepSelected()) {
            return [];
        }

        const ar1 = ko.utils.arrayFilter(params.items(), function (item) {
            return params.selectedStep().validatableIds().indexOf(item.id()) > -1;
        })

        const ar = ar1.map((value) => {
            return value.value;
        });

        vm.validatables(ar);
    });

    vm.getTopOffset = function getTopOffset(containerEl, relativeEl, controlEl) {

        const labelOffset = 25;
        const controlElTop = controlEl.getBoundingClientRect().top;

        if (containerEl) {
            const containerTop = containerEl.getBoundingClientRect().top;
            const absoluteControlElTop = controlElTop + containerEl.scrollTop;

            return absoluteControlElTop - containerTop - labelOffset - relativeEl.offsetHeight;
        } else {
            const absoluteControlElTop = controlElTop + window.scrollY;

            return absoluteControlElTop - labelOffset;
        }
    }

    return vm;
}

function mapStateToParams({ rulesEngine, visibleKeys, selectedStep, items }) {

    const steps = ko.pureComputed(() => {
        const stepItems = ko.utils.arrayFilter(items(), function (item) {
            return item.type === 'step';
        });

        return stepItems.map((step, index) => {
            const stepNumber = index + 1;

            const active = selectedStep() !== undefined && selectedStep().ordinal() >= step.ordinal();

            return { step, stepNumber, active }
        })
    });

    return { rulesEngine, visibleKeys, selectedStep, steps, items };
}

export default connect(mapStateToParams)(stepBreadcrumbViewModel);
