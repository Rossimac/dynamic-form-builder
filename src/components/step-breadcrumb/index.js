import ko from 'knockout';
import template from './step-breadcrumb.template.html';
import viewModel from './step-breadcrumb.viewmodel.js';
import './step-breadcrumb.styles.scss';
import '../step-progress';

let synchronous = true;

ko.components.register('step-breadcrumb', { template, viewModel, synchronous });
