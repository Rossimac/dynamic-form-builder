import ko from 'knockout';
import template from './collection-summary.template.html';
import viewModel from './collection-summary.viewmodel.js';
import './collection-summary.styles.scss';
import '../collection-summary-item';

let synchronous = true;

ko.components.register('collection-summary', { template, viewModel, synchronous });
