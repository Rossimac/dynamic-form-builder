import ko from 'knockout';
import { connect } from 'knockout-store';

function collectionSummaryViewModel(params) {
    const vm = this;
    
    vm.items = params.items;
    vm.collectionQuestion = params.collectionQuestion;

    vm.previousData = ko.pureComputed(() => {

        let previousDataIds = [];

        vm.items().forEach(element => {
            if (element.groupKey === vm.collectionQuestion.key && previousDataIds.indexOf(element.groupId) === -1 && element.saved() === true) {
                previousDataIds.push(element.groupId)
            }
        });
        
        return previousDataIds;
    });

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    var collectionId = id.replace('collectionSummary-', '');

    const collectionQuestion = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === collectionId;
    });

    return { items, collectionQuestion };
}

export default connect(mapStateToParams, mergeParams)(collectionSummaryViewModel);
