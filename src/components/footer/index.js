import ko from 'knockout';
import template from './footer.template.html';
import viewModel from './footer.viewmodel.js';
import './footer.styles.scss';

let synchronous = true;

ko.components.register('sticky-footer', { template, viewModel, synchronous });
