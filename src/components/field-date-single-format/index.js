import ko from 'knockout';
import template from './field-date-single-format.template.html';
import viewModel from './field-date-single-format.viewmodel.js';
import './field-date-single-format.styles.scss';

let synchronous = true;

ko.components.register('field-date-single-format', { template, viewModel, synchronous });