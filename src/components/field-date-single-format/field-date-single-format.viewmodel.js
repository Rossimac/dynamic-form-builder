import ko from 'knockout';
import { connect } from 'knockout-store';
import { DateTime } from 'luxon';

function fieldDateSingleFormatViewModel(params) {
    const vm = this;

    vm.items = params.items;
    vm.visibleKeys = params.visibleKeys;

    vm.value = params.question.value;
    vm.options = params.question.options;

    if (vm.options === undefined) {
        console.log('No options defined for Date');
        return;
    }

    if (params.minQuestion?.value() !== undefined) {
        vm.minQuestionSubscription = params.minQuestion.value.subscribe(function (newValue) {

            if (vm.value() === undefined) {
                return;
            }

            if (vm.value() < newValue) {

                vm.value(undefined);

                //TODO: it would be neat to raise these as events to the user if it's 
                //not immediately obvious something has changed (i.e. on a different screen)
            }
        });
    }

    if (params.maxQuestion?.value() !== undefined) {
        vm.maxQuestionSubscription = params.maxQuestion.value.subscribe(function (newValue) {

            if (vm.value() === undefined) {
                return;
            }

            if (vm.value() > newValue) {
                vm.value(undefined);

                //TODO: it would be neat to raise these as events to the user if it's 
                //not immediately obvious something has changed (i.e. on a different screen)
            }
        });
    }

    vm.placeholder = vm.options.placeholder
    vm.caption = vm.options.caption ?? "Please select";

    const initialValue = vm.options.defaultValue;

    function setDate(newValue) {

        const newValueAsDate = DateTime.fromISO(newValue);
        const newValueAsISO = newValueAsDate.toISO();
        const newStartOfDayValueAsISO = newValueAsDate.startOf('day').toISO();

        if (newValueAsDate.isValid) {
            if (newValue !== newValueAsISO || (newValue === newValueAsISO) && newValueAsISO !== newStartOfDayValueAsISO) {
                vm.value(newStartOfDayValueAsISO);
            }
        } else {
            vm.value(undefined);
        }
    }

    vm.valueSubscription = vm.value.subscribe(function (newValue) {
        if (newValue !== undefined) {
            setDate(newValue);
        }
    });

    vm.dateOptions = ko.pureComputed(() => {

        let minDate;
        if (params.mininiumDateBaseOnQuestion?.value() !== undefined) {
            minDate = DateTime.fromISO(params.mininiumDateBaseOnQuestion.value());
        } else {
            minDate = DateTime.now();
        }

        if (vm.options.dateDivider === 'days') {
            minDate = minDate.startOf('day');
        } else {
            minDate = minDate.toISO();
        }

        if (vm.options.minimumDaysRelative !== undefined) {
            minDate = minDate.plus({ days: vm.options.minimumDaysRelative });
        }

        if (vm.options.minimumMonthsRelative !== undefined) {
            minDate = minDate.plus({ months: vm.options.minimumMonthsRelative });
        }

        if (vm.options.minimumYearsRelative !== undefined) {
            minDate = minDate.plus({ years: vm.options.minimumYearsRelative });
        }

        let maxDate;
        if (params.maximumDateBaseOnQuestion?.value() !== undefined) {
            maxDate = DateTime.fromISO(params.maximumDateBaseOnQuestion.value());
        } else {
            maxDate = DateTime.now();
        }

        if (vm.options.maximumDaysRelative !== undefined) {
            maxDate = maxDate.plus({ days: vm.options.maximumDaysRelative });
        }

        if (vm.options.maximumDaysRelative !== undefined) {
            maxDate = maxDate.plus({ months: vm.options.maximumMonthsRelative });
        }

        if (vm.options.maximumYearsRelative !== undefined) {
            maxDate = maxDate.plus({ years: vm.options.maximumYearsRelative });
        }

        const daysDifference = maxDate.diff(minDate, vm.options.dateDivider)

        var dateOptions = [];

        for (let index = 0; index < daysDifference.days; index++) {

            const currentDate = minDate.plus({ days: index });

            const appendTodayAndTomorrow = vm.options.appendTodayAndTomorrow !== undefined && (index === 0 || index === 1);
            const formattedCurrentDate = formatDate(currentDate, vm.options.format, appendTodayAndTomorrow);

            dateOptions.push(new vm.optionValue(formattedCurrentDate, currentDate.toISO()));
        }

        return dateOptions;
    });

    function formatDate(dateValue, format, appendTodayAndTomorrow) {

        let suffix;
        if (appendTodayAndTomorrow === true) {
            suffix = ' (' + titleCase(dateValue.toRelativeCalendar()) + ')';
        }
        else {
            suffix = '';
        }

        const startPosition = format.indexOf('ddd');

        if (startPosition === -1) {
            return dateValue.toFormat(format) + suffix;
        }

        const leftSide = dateValue.toFormat(format.substring(0, startPosition + 1));

        const middle = getNumberSuffix(dateValue.day);

        const rightSide = dateValue.toFormat(format.substring(startPosition + 3, format.length));

        return leftSide + middle + rightSide + suffix;
    }

    function getNumberSuffix(num) {
        const th = 'th'
        const rd = 'rd'
        const nd = 'nd'
        const st = 'st'

        if (num === 11 || num === 12 || num === 13) return th

        let lastDigit = num.toString().slice(-1)

        switch (lastDigit) {
            case '1': return st
            case '2': return nd
            case '3': return rd
            default: return th
        }
    }

    function titleCase(str) {
        return str.toLowerCase().split(' ').map(function (word) {
            return (word.charAt(0).toUpperCase() + word.slice(1));
        }).join(' ');
    }

    vm.optionValue = function (text, value) {
        this.text = text;
        this.value = value;
    };

    vm.setOptionDisable = function (option, item) {
        if (item !== undefined) {
            return;
        }

        ko.applyBindingsToNode(
            option,
            {
                disable: true,
                hidden: true
            },
            item);
    }

    if (vm.value() !== undefined) {
        setDate(vm.value());
    }
    else {
        vm.value(initialValue);
    }

    return vm;
}

fieldDateSingleFormatViewModel.prototype.dispose = function () {
    this.valueSubscription.dispose();
    this.minQuestionSubscription?.dispose();
    this.maxQuestionSubscription?.dispose();
}

function mapStateToParams({ rulesEngine, items, visibleKeys }) {
    return { rulesEngine, items, visibleKeys };
}

function mergeParams({ rulesEngine, items, visibleKeys }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    let minQuestion = undefined;
    if (question.options?.min?.type === 'question') {
        minQuestion = ko.utils.arrayFirst(items(), function (item) {
            return item.id() === question.options.min.questionId;
        });
    }

    let maxQuestion = undefined;
    if (question.options?.max?.type === 'question') {
        maxQuestion = ko.utils.arrayFirst(items(), function (item) {
            return item.id() === question.options.max.questionId;
        });
    }

    return { rulesEngine, items, visibleKeys, question, minQuestion, maxQuestion };
}

export default connect(mapStateToParams, mergeParams)(fieldDateSingleFormatViewModel);
