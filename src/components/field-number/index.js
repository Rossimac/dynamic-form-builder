import ko from 'knockout';
import template from './field-number.template.html';
import viewModel from './field-number.viewmodel.js';
import './field-number.styles.scss';

let synchronous = true;

ko.components.register('field-number', { template, viewModel, synchronous });
