import ko from 'knockout';
import { connect } from 'knockout-store';

function fieldNumberViewModel(params) {
    const vm = this;
    vm.value = params.question.value;
    vm.textAlign = params.textAlign;

    vm.hasTextAlign = ko.pureComputed(() => vm.textAlign !== undefined);

    if (params.question.options !== undefined) {
        vm.placeholder = params.question.options.placeholder;
    }

    vm.hasMin = ko.pureComputed(() =>
        params.question.options?.min !== undefined);

    vm.min = params.question.options?.min;

    vm.hasMax = ko.pureComputed(() =>
        params.question.options?.max !== undefined);

    vm.max = params.question.options?.max;

    vm.numberSubscription = vm.value.subscribe(function (newValue) {
        const integerNewValue = parseInt(newValue);

        if (!isNaN(integerNewValue) && newValue === '' + integerNewValue) {
            return;
        }

        if (isNaN(integerNewValue)) {
            vm.value(undefined);
            return;
        }

        vm.value(integerNewValue);
    });

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id, textAlign }) {

    let question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    return { question, textAlign };
}

export default connect(mapStateToParams, mergeParams)(fieldNumberViewModel);
