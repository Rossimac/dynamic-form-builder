import ko from 'knockout';
import viewModel from './form-layout.viewmodel';
import template from './form-layout.template.html';
import './form-layout.styles.scss';
import '../step';

let synchronous = true;

ko.components.register('form-layout', { template, viewModel, synchronous });
