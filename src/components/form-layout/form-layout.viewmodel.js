import ko from 'knockout';
import { connect } from 'knockout-store';

function formLayoutViewModel(params) {
    const vm = this;

    vm.steps = params.steps;
    
    return vm;
}

function mapStateToParams({ items }) {

    let steps = ko.pureComputed(() => {
        return ko.utils.arrayFilter(items(), function (item) {
            return item.type === 'step';
        });
    });

    return { steps };
}

export default connect(mapStateToParams)(formLayoutViewModel);
