import ko from 'knockout';
import template from './vehicle-find.template.html';
import viewModel from './vehicle-find.viewmodel.js';
import './vehicle-find.styles.scss';

let synchronous = true;

ko.components.register('vehicle-find', { template, viewModel, synchronous });