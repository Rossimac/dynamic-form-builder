import ko from 'knockout';
import { connect } from 'knockout-store';
import axios from 'axios';
import dotProp from 'dot-prop';

function vehicleFindViewModel(params) {
    const vm = this;

    vm.id = params.question.id();
    vm.question = params.question.value;
    vm.mappedQuestions = params.mappedQuestions;

    async function findCar() {

        if (vm.question() === undefined || vm.question() === '') {
            vm.question.isModified(true);
            return;
        }

        try {
            const response = await axios.get('https://uk1.ukvehicledata.co.uk/api/datapackage/VehicleData?v=2&api_nullitems=1&auth_apikey=313076c6-c570-4d58-9567-2f4bb98d3f2b&user_tag=da63oae&key_VRM=' + vm.question());

            const vehicleData = response.data;

            vm.mappedQuestions.forEach(mappedQuestion => {
        
                let result = dotProp.get(vehicleData.Response, mappedQuestion.key);
        
                mappedQuestion.value(result);
            });
        } catch (error) {
            console.error(error);
        }
    }
    vm.findCarClicked = findCar;

    function findByMakeAndModel() {

        if (vm.question() !== undefined) {
            vm.question(undefined);
            
            if(vm.question.isModified !== undefined){
                vm.question.isModified(false);
            }
            return;
        }
    }
    vm.findByMakeAndModelClicked = findByMakeAndModel;

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    let mappedQuestions = [];

    if (question.options !== undefined && question.options.mappedQuestions !== undefined) {
        mappedQuestions = items().reduce((c, n) => {
            var mappedQuestionsForItem = question.options.mappedQuestions.filter(function (item) {
                return item.to === n.key;
            });

            mappedQuestionsForItem.forEach(mappedQuestionForItem => {
                c.push({ key: mappedQuestionForItem.from, value: n.value });
            });

            return c;
        }, []);
    }

    return {
        mappedQuestions: mappedQuestions,
        question: question,
    };
}

export default connect(mapStateToParams, mergeParams)(vehicleFindViewModel);
