import ko from 'knockout';
import template from './collection-summary-item.template.html';
import viewModel from './collection-summary-item.viewmodel.js';
import './collection-summary-item.styles.scss';

let synchronous = true;

ko.components.register('collection-summary-item', { template, viewModel, synchronous });
