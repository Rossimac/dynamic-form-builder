import ko from 'knockout';
import { connect } from 'knockout-store';

function collectionSummaryItemViewModel(params) {
    const vm = this;

    vm.items = params.items;
    vm.groupId = params.groupId;
    vm.collectionQuestion = params.collectionQuestion;

    let options = vm.collectionQuestion.collection.options;

    vm.summaryTtile = ko.pureComputed(() => options.summaryTitle.template);
    vm.summaryMutedDetail = ko.pureComputed(() => options.summarySubtitle.template);
    vm.summaryDetail = ko.pureComputed(() => options.summaryDetail.template);

    vm.otherGroupDataExists = ko.pureComputed(() => {

        let otherGroupData = ko.utils.arrayFirst(vm.items(), function (element) {
            return element.groupKey === vm.collectionQuestion.key;
        });

        return otherGroupData !== undefined;
    });

    vm.isGroupRemoved = ko.pureComputed(() => {

        let groupDataRemoved = ko.utils.arrayFirst(vm.items(), function (item) {
            return item.groupId === vm.groupId;
        });

        return groupDataRemoved.removed() === true;
    });

    function remove() {
        const groupItems = ko.utils.arrayFilter(vm.items(), function (item) {

            return item.groupId == vm.groupId;
        });

        ko.utils.arrayForEach(groupItems, function (item) {
            vm.items.remove(item);
            item.removed(true);
            vm.items.push(item);
        });
    };
    vm.removeClicked = remove;

    function undoRemoval() {
        const groupItems = ko.utils.arrayFilter(vm.items(), function (item) {
            return item.groupId == vm.groupId;
        });
        ko.utils.arrayForEach(groupItems, function (item) {

            vm.items.remove(item);
            item.removed(false);
            vm.items.push(item);
        });
    };
    vm.undoClicked = undoRemoval;

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { groupId }) {

    const nestedQuestion = ko.utils.arrayFirst(items(), function (item) {
        return item.groupId === groupId;
    });

    const collectionQuestion = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === nestedQuestion.groupKey;
    });

    return { items, groupId, collectionQuestion };
}

export default connect(mapStateToParams, mergeParams)(collectionSummaryItemViewModel);
