import ko from 'knockout';
import viewModel from './collection-data-capture.viewmodel';
import template from './collection-data-capture.template.html';
import './collection-data-capture.styles.scss';
import '../question';

let synchronous = true;

ko.components.register('collection-data-capture', { viewModel, template, synchronous });
