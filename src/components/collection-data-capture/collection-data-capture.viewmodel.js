import ko from 'knockout';
import { connect } from 'knockout-store';
import { v4 as uuid } from 'uuid';
import traverseLayout from '../../functions/layoutTaverser';

function collectionDataCaptureViewModel(params) {
    const vm = this;

    vm.items = params.items;
    vm.collectionQuestion = params.collectionQuestion;

    vm.previousData = ko.pureComputed(() => {

        let previousDataIds = [];

        vm.items().forEach(element => {
            if (element.groupKey === vm.collectionQuestion.key && previousDataIds.indexOf(element.groupId) === -1 && element.saved() === true) {
                previousDataIds.push(element.groupId)
            }
        });

        return previousDataIds;
    });

    vm.currentData = ko.pureComputed(() =>
        vm.items().filter(element =>
            stringStartsWith(element.id(), vm.collectionQuestion.collection.latestGroupId())
        )
    );

    var stringStartsWith = function (string, startsWith) {
        string = string || "";
        if (startsWith.length > string.length)
            return false;
        return string.substring(0, startsWith.length) === startsWith;
    };

    vm.showSaveButton = ko.pureComputed(() =>
        vm.collectionQuestion.collection.options.autoSave === false
    );
    function save() {
        if (!vm.validate.isValid()) {
            vm.validate.errors.showAllMessages();
            return;
        }

        vm.currentData().forEach(element => {
            element.saved(true);
        });
        vm.collectionQuestion.collection.showDataCapture(false);
        vm.collectionQuestion.collection.addAnother(false);
    }
    vm.saveClicked = save;

    vm.showCancelButton = ko.pureComputed(() => {
        return vm.previousData().length > 0;
    });
    function cancel() {
        vm.collectionQuestion.collection.addAnother(false);
        vm.collectionQuestion.collection.showDataCapture(false);

        vm.items.remove(function (item) {
            return item.groupKey == vm.collectionQuestion.key && item.saved() === false;
        });
    }
    vm.cancelClicked = cancel;

    vm.showRemovalWarning = ko.pureComputed(() => {
        return false;
    });

    vm.questions = ko.pureComputed(() => {
        return params.questionsKeys();
    });

    vm.validatables = ko.observableArray();

    vm.stepNavConfig = ko.computed(() => {

        const ar = vm.currentData().map((value) => {
            return value.value;
        });

        vm.validatables(ar);
    });

    vm.validate = ko.validatedObservable({
        items: vm.validatables
    });

    return vm;
}

function mapStateToParams({ selectedStep, items }) {
    return { selectedStep, items };
}

function mergeParams({ selectedStep, items }, { id }) {

    const collectionQuestion = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id();
    });

    let questionsKeys = null;
    let latestGroupId = uuid();

    if (collectionQuestion.collection.items !== undefined) {

        questionsKeys = ko.observableArray(collectionQuestion.collection.items.map((value, itemIndex) => {
            return traverseLayout(items, value, itemIndex, collectionQuestion.key, latestGroupId, selectedStep);
        }).flat());
    }

    collectionQuestion.collection.latestGroupId(latestGroupId);

    return { collectionQuestion, questionsKeys, items };
}

export default connect(mapStateToParams, mergeParams)(collectionDataCaptureViewModel);
