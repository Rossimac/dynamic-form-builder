import ko from 'knockout';
import { connect } from 'knockout-store';

function fieldListViewModel(params) {
    const vm = this;
    
    vm.assumptions = params.assumptions;

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    if(question === undefined){
        console.log('Assumption List ID not found: ' + id);
        return;
    }

    if(question.options === undefined || question.options.assumptions === undefined){
        console.log('Assumption List does not have any Assumtions');
        return;
    }    

    return { assumptions: question.options.assumptions };
}

export default connect(mapStateToParams, mergeParams)(fieldListViewModel);
