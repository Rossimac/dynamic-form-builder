import ko from 'knockout';
import template from './field-assumption-list.template.html';
import viewModel from './field-assumption-list.viewmodel.js';
import './field-assumption-list.styles.scss';
import '../field-assumption';

let synchronous = true;

ko.components.register('field-assumption-list', { template, viewModel, synchronous });
