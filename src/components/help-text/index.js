import ko from 'knockout';
import template from './help-text.template.html';
import viewModel from './help-text.viewmodel.js';
import './help-text.styles.scss';

let synchronous = true;

ko.components.register('help-text', { template, viewModel, synchronous });