import ko from 'knockout';
import { connect } from 'knockout-store';

function helpTextViewModel(params) {
    const vm = this;
    vm.id = params.id;

    vm.addEventListenerAdded = false;

    vm.linkText = ko.observable('Need help?');
    function toggleLinkText() {
        if (vm.addEventListenerAdded === true) {
            return;
        }
        vm.addEventListenerAdded = true;

        var myCollapsible = document.getElementById('help-text-collapse-' + vm.id)
        myCollapsible.addEventListener('shown.bs.collapse', function () {
            vm.linkText('Hide help?');
        });

        myCollapsible.addEventListener('hidden.bs.collapse', function () {
            vm.linkText('Need help?');
        });

        return;
    }
    vm.linkClicked = toggleLinkText;

    vm.helpText = params.helpText;

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    return {
        helpText: question.options.helpText,
        id: id,
    };
}

export default connect(mapStateToParams, mergeParams)(helpTextViewModel);
