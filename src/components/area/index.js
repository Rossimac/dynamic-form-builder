import ko from 'knockout';
import viewModel from './area.viewmodel';
import template from './area.template.html';
import './area.styles.scss';
import '../question';

let synchronous = true;

ko.components.register('area-component', { viewModel, template, synchronous });
