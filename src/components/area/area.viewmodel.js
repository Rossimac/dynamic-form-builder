import ko from 'knockout';
import { connect } from 'knockout-store';

function areaViewModel(params) {
    const vm = this;
    vm.areaId = params.area.id();

    vm.introVisible = ko.pureComputed(() => {
        return params.area.intro !== undefined;
    });

    if(vm.introVisible()){
        vm.introHeader = params.area.intro.header;
        vm.introSubHeader = params.area.intro.subHeader;
        vm.introHint = params.area.intro.hint;
    }

    vm.title = ko.pureComputed(() => {
        return params.area.name;
    });
    
    vm.questions = ko.pureComputed(() => {
        return params.area.items();
    });

    vm.hasNotification = ko.pureComputed(() => {
        return params.area.options !== undefined && params.area.options.notification !== undefined;
    });
    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {
    const area = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    return { area };
}

export default connect(mapStateToParams, mergeParams)(areaViewModel);
