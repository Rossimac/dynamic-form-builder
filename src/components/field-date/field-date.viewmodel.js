import ko from 'knockout';
import { connect } from 'knockout-store';
import { DateTime } from 'luxon';

function fieldDateViewModel(params) {
    const vm = this;

    vm.items = params.items;
    vm.visibleKeys = params.visibleKeys;

    vm.value = params.question.value;
    vm.options = params.question.options;

    vm.confirmButtonVisible = ko.pureComputed(() => vm.options?.confirmFirstVisit ?? false);

    if (vm.options === undefined) {
        console.log('No options defined for Date');
        return;
    }

    vm.displayAsLists = vm.options.displayAsLists;

    if (params.minQuestion?.value() !== undefined) {
        vm.minQuestionSubscription = params.minQuestion.value.subscribe(function (newValue) {

            if (vm.value() === undefined) {
                return;
            }

            if (vm.value() < newValue) {
                vm.dayValue(undefined);
                vm.monthValue(undefined);
                vm.yearValue(undefined);

                vm.value(undefined);

                //TODO: it would be neat to raise these as events to the user if it's 
                //not immediately obvious something has changed (i.e. on a different screen)
            }
        });
    }

    if (params.maxQuestion?.value() !== undefined) {
        vm.maxQuestionSubscription = params.maxQuestion.value.subscribe(function (newValue) {

            if (vm.value() === undefined) {
                return;
            }

            if (vm.value() > newValue) {
                vm.dayValue(undefined);
                vm.monthValue(undefined);
                vm.yearValue(undefined);

                vm.value(undefined);

                //TODO: it would be neat to raise these as events to the user if it's 
                //not immediately obvious something has changed (i.e. on a different screen)
            }
        });
    }

    vm.dayValue = ko.observable();
    vm.dayShown = vm.options.day?.visible ?? false;
    vm.dayPlaceholder = vm.options.day?.placeholder ?? "DD";

    if (vm.dayShown === true) {
        if (vm.options.day?.defaultValue !== undefined) {
            vm.dayInitialValue = vm.options.day?.defaultValue;
        }
    } else {
        vm.dayInitialValue = vm.options.day.defaultValue;
    }

    vm.monthValue = ko.observable();
    vm.monthShown = vm.options.month?.visible ?? false;
    vm.monthPlaceholder = vm.options.month?.placeholder ?? "MM";

    if (vm.monthShown === true) {
        if (vm.options.month?.defaultValue !== undefined) {
            vm.monthInitialValue = vm.options.month?.defaultValue;
        }
    } else {
        vm.monthInitialValue = vm.options.month.defaultValue;
    }

    vm.yearValue = ko.observable();
    vm.yearShown = vm.options.year?.visible ?? false;
    vm.yearCaption = vm.options.year?.caption;
    vm.yearPlaceholder = vm.options.year?.placeholder ?? "YYYY";

    if (vm.yearShown === true) {
        if (vm.options.year?.defaultValue !== undefined) {
            vm.yearInitialValue = vm.options.year?.defaultValue;
        }
    } else {
        vm.yearInitialValue = vm.options.year.defaultValue;
    }

    function setNewDate() {
        if (vm.dayValue() === undefined || vm.monthValue() === undefined || vm.yearValue() === undefined) {
            return;
        }

        const newDate = DateTime.local(parseInt(vm.yearValue()), parseInt(vm.monthValue()), parseInt(vm.dayValue()));

        if (newDate.isValid) {
            vm.value(newDate.toISO());
        } else {
            vm.value(undefined);

            if (newDate.invalidReason === 'unit out of range') {

                if (newDate.invalidExplanation === 'you specified NaN (of type number) as a month, which is invalid') {
                    if (vm.monthShown) {
                        vm.value.setError(`Oops - are you sure? This doesn't look right`);
                        vm.value.isModified(true);
                    } else {
                        vm.monthValue(vm.monthInitialValue);
                    }
                }

                if (newDate.invalidExplanation.endsWith('(of type number) as a day, which is invalid')) {

                    const testDate = DateTime.local(parseInt(vm.yearValue()), parseInt(vm.monthValue()), 1);

                    if (testDate.isValid) {
                        if ((vm.dayValue() > testDate.daysInMonth)) {
                            if (vm.dayShown) {
                                vm.value.setError(`Oops - are you sure? This doesn't look right`);
                                vm.value.isModified(true);
                            } else {
                                vm.dayValue(vm.dayInitialValue);
                            }
                        }
                    }
                }
            }

        }
    }

    vm.daySubscription = vm.dayValue.subscribe(function () {
        setNewDate();
    });

    vm.monthSubscription = vm.monthValue.subscribe(function () {
        setNewDate();
    });

    vm.yearSubscription = vm.yearValue.subscribe(function (newValue) {

        if (newValue && newValue.length === 2) {
            const currentDate = DateTime.now();

            let dayValue = 1;
            let monthValue = 1;
            if (vm.dayValue() !== undefined && vm.monthValue() !== undefined) {
                dayValue = parseInt(vm.dayValue());
                monthValue = parseInt(vm.monthValue());
            }

            let newDate = DateTime.local(parseInt(currentDate.year.toString().substring(0, 2) + newValue), monthValue, dayValue);

            if (newDate.isValid === false) {
                newDate = DateTime.local(parseInt(currentDate.year.toString().substring(0, 2) + newValue), 1, 1);
            }

            if (newDate > currentDate) {
                const yearOneHunderdYearsAgo = DateTime.now().minus({ years: 100 }).year;

                newDate = DateTime.local(parseInt(yearOneHunderdYearsAgo.toString().substring(0, 2) + newValue), 1, 1);
            }

            vm.yearValue(newDate.year);
            return;
        }

        setNewDate();
    });

    function setDate(newValue) {
        const newValseAsISO = DateTime.fromISO(newValue);

        let newDayValue;
        if (vm.dayShown) {
            newDayValue = newValseAsISO.day;
        } else {
            newDayValue = vm.dayInitialValue;
        }

        vm.dayValue(newDayValue.toLocaleString('en-GB', {
            minimumIntegerDigits: 2
        }));

        let newMonthValue;
        if (vm.monthShown) {
            newMonthValue = newValseAsISO.month;
        } else {
            newMonthValue = vm.monthInitialValue;
        }

        vm.monthValue(newMonthValue.toLocaleString('en-GB', {
            minimumIntegerDigits: 2
        }));

        vm.yearValue(newValseAsISO.year);
    }

    vm.valueSubscription = vm.value.subscribe(function (newValue) {
        if (newValue !== undefined) {
            setDate(newValue);
        }
    });

    vm.dayValue(vm.dayInitialValue);
    vm.monthValue(vm.monthInitialValue);
    vm.yearValue(vm.yearInitialValue);

    vm.optionValue = function (text, value) {
        this.text = text;
        this.value = value;
    };

    vm.dayOptions = ko.pureComputed(() => {

        let minDay = 1;
        let maxDay = 31;

        if (vm.monthValue() !== undefined) {
            maxDay = DateTime.local(DateTime.now().year, vm.monthValue()).daysInMonth;

            if (vm.monthValue() !== undefined && vm.yearValue() !== undefined) {


                let minQuestionDate;
                if (params.minQuestion?.value() !== undefined) {
                    minQuestionDate = DateTime.fromISO(params.minQuestion.value());
                } else {
                    minQuestionDate = DateTime.now();
                }

                if (vm.options.minDays !== undefined) {
                    minQuestionDate = minQuestionDate.plus({ days: vm.options.minDays });
                }

                if (vm.options.minMonths !== undefined) {
                    minQuestionDate = minQuestionDate.plus({ months: vm.options.minMonths });
                }

                if (vm.options.minYears !== undefined) {
                    minQuestionDate = minQuestionDate.plus({ years: vm.options.minYears });
                }

                let maxQuestionDate;
                if (params.maxQuestion?.value() !== undefined) {
                    maxQuestionDate = DateTime.fromISO(params.maxQuestion.value());
                } else {
                    maxQuestionDate = DateTime.now();
                }

                if (vm.options.maxDays !== undefined) {
                    maxQuestionDate = maxQuestionDate.plus({ days: vm.options.maxDays });
                }

                if (vm.options.maxMonths !== undefined) {
                    maxQuestionDate = maxQuestionDate.plus({ months: vm.options.maxMonths });
                }

                if (vm.options.maxYears !== undefined) {
                    maxQuestionDate = maxQuestionDate.plus({ years: vm.options.maxYears });
                }

                const monthAndYearDateMin = DateTime.local(vm.yearValue(), vm.monthValue());
                const monthAndYearDateMax = DateTime.local(vm.yearValue(), vm.monthValue() + 1);

                if (monthAndYearDateMin <= minQuestionDate) {
                    minDay = minQuestionDate.day;
                }

                if (monthAndYearDateMax >= maxQuestionDate) {
                    maxDay = maxQuestionDate.day;
                }
            }
        }

        var dayOptions = [];

        for (let currentDay = minDay; currentDay < maxDay + 1; currentDay++) {
            dayOptions.push(new vm.optionValue(currentDay.toLocaleString({
                minimumIntegerDigits: 2
            }), currentDay));
        }

        return dayOptions;
    });

    vm.monthOptions = ko.pureComputed(() => {

        let minMonth = 0;
        let maxMonth = 12;

        if (vm.yearValue() !== undefined) {

            let minQuestionDate;
            if (params.minQuestion?.value() !== undefined) {
                minQuestionDate = DateTime.fromISO(params.minQuestion.value());
            } else {
                minQuestionDate = DateTime.now();
            }

            if (vm.options.minDays !== undefined) {
                minQuestionDate = minQuestionDate.plus({ days: vm.options.minDays });
            }

            if (vm.options.minMonths !== undefined) {
                minQuestionDate = minQuestionDate.plus({ months: vm.options.minMonths });
            }

            if (vm.options.minYears !== undefined) {
                minQuestionDate = minQuestionDate.plus({ years: vm.options.minYears });
            }

            let maxQuestionDate;
            if (params.maxQuestion?.value() !== undefined) {
                maxQuestionDate = DateTime.fromISO(params.maxQuestion.value());
            } else {
                maxQuestionDate = DateTime.now();
            }

            if (vm.options.maxDays !== undefined) {
                maxQuestionDate = maxQuestionDate.plus({ days: vm.options.maxDays });
            }

            if (vm.options.maxMonths !== undefined) {
                maxQuestionDate = maxQuestionDate.plus({ months: vm.options.maxMonths });
            }

            if (vm.options.maxYears !== undefined) {
                maxQuestionDate = maxQuestionDate.plus({ years: vm.options.maxYears });
            }

            if (vm.yearValue() <= minQuestionDate.year) {
                minMonth = minQuestionDate.month - 1;
            }

            if (vm.yearValue() >= maxQuestionDate.year) {
                maxMonth = maxQuestionDate.month;
            }
        }

        var months = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];

        var monthOptions = [];

        for (let index = minMonth; index < maxMonth; index++) {
            monthOptions.push(new vm.optionValue([months[index]], index + 1));
        }

        return monthOptions;
    });

    vm.yearOptions = ko.pureComputed(() => {

        let minQuestionDate;
        if (params.minQuestion?.value() !== undefined) {
            minQuestionDate = DateTime.fromISO(params.minQuestion.value());
        } else {
            minQuestionDate = DateTime.now();
        }

        if (vm.options.minDays !== undefined) {
            minQuestionDate = minQuestionDate.plus({ days: vm.options.minDays });
        }

        if (vm.options.minMonths !== undefined) {
            minQuestionDate = minQuestionDate.plus({ months: vm.options.minMonths });
        }

        if (vm.options.minYears !== undefined) {
            minQuestionDate = minQuestionDate.plus({ years: vm.options.minYears });
        }

        let maxQuestionDate;
        if (params.maxQuestion?.value() !== undefined) {
            maxQuestionDate = DateTime.fromISO(params.maxQuestion.value());
        } else {
            maxQuestionDate = DateTime.now();
        }

        if (vm.options.maxDays !== undefined) {
            maxQuestionDate = maxQuestionDate.plus({ days: vm.options.maxDays });
        }

        if (vm.options.maxMonths !== undefined) {
            maxQuestionDate = maxQuestionDate.plus({ months: vm.options.maxMonths });
        }

        if (vm.options.maxYears !== undefined) {
            maxQuestionDate = maxQuestionDate.plus({ years: vm.options.maxYears });
        }

        var yearOptions = [];

        for (let currentYear = minQuestionDate.year; currentYear < maxQuestionDate.year + 1; currentYear++) {
            yearOptions.push(new vm.optionValue(currentYear.toString(), currentYear));
        }

        return yearOptions;
    });

    vm.setOptionDisable = function (option, item) {
        if (item !== undefined) {
            return;
        }

        ko.applyBindingsToNode(
            option,
            {
                disable: true,
                hidden: true
            },
            item);
    }

    if (vm.value() !== undefined) {
        setDate(vm.value());
    }

    return vm;
}

fieldDateViewModel.prototype.dispose = function () {
    this.daySubscription.dispose();
    this.monthSubscription.dispose();
    this.yearSubscription.dispose();
    this.valueSubscription.dispose();
    this.minQuestionSubscription?.dispose();
    this.maxQuestionSubscription?.dispose();
}

function mapStateToParams({ rulesEngine, items, visibleKeys }) {
    return { rulesEngine, items, visibleKeys };
}

function mergeParams({ rulesEngine, items, visibleKeys }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    let minQuestion = undefined;
    if (question.options?.min?.type === 'question') {
        minQuestion = ko.utils.arrayFirst(items(), function (item) {
            return item.id() === question.options.min.questionId;
        });
    }

    let maxQuestion = undefined;
    if (question.options?.max?.type === 'question') {
        maxQuestion = ko.utils.arrayFirst(items(), function (item) {
            return item.id() === question.options.max.questionId;
        });
    }

    return { rulesEngine, items, visibleKeys, question, minQuestion, maxQuestion };
}

export default connect(mapStateToParams, mergeParams)(fieldDateViewModel);
