import ko from 'knockout';
import template from './field-date.template.html';
import viewModel from './field-date.viewmodel.js';
import './field-date.styles.scss';

let synchronous = true;

ko.components.register('field-date', { template, viewModel, synchronous });