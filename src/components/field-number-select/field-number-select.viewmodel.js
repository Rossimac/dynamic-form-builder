import ko from 'knockout';
import { connect } from 'knockout-store';

function fieldNumberSelectViewModel(params) {
    const vm = this;

    vm.id = params.question.id();
    vm.value = params.question.value;

    if (params.question.options !== undefined) {
        vm.placeholder = params.question.options.placeholder ?? "Select";
        vm.lowerLimit = params.question.options.lowerLimit;
        vm.upperLimit = params.question.options.upperLimit;
    }

    vm.showMinus = ko.pureComputed(() => {

        if (vm.lowerLimit && vm.value() <= vm.lowerLimit) {
            return false;
        }

        return true;
    });

    function decrementValue() {

        let currentIntValue = parseInt(vm.value());

        vm.value(currentIntValue - 1);
    }
    vm.minusClicked = decrementValue;

    vm.showPlus = ko.pureComputed(() => {

        if (vm.upperLimit && vm.value() >= vm.upperLimit) {
            return false;
        }

        return true;
    });

    function increaseValue() {

        let currentIntValue = parseInt(vm.value());

        vm.value(currentIntValue + 1);
    }
    vm.plusClicked = increaseValue;

    vm.value.subscribe(newValue => {

        if (vm.lowerLimit && newValue < vm.lowerLimit) {
            vm.value(vm.lowerLimit);
            return;
        }

        if (vm.upperLimit && newValue > vm.upperLimit) {
            vm.value(vm.upperLimit);
            return;
        }
    });

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    let question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    return { question };
}

export default connect(mapStateToParams, mergeParams)(fieldNumberSelectViewModel);
