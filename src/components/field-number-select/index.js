import ko from 'knockout';
import template from './field-number-select.template.html';
import viewModel from './field-number-select.viewmodel.js';
import './field-number-select.styles.scss';
import '../field-number';

let synchronous = true;

ko.components.register('field-number-select', { template, viewModel, synchronous });
