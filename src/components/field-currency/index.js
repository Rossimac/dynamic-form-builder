import ko from 'knockout';
import template from './field-currency.template.html';
import viewModel from './field-currency.viewmodel.js';
import './field-currency.styles.scss';
import '../field-number';

let synchronous = true;

ko.components.register('field-currency', { template, viewModel, synchronous });