import ko from 'knockout';
import { connect } from 'knockout-store';

function fieldTextViewModel(params) {
    const vm = this;
    
    vm.id = params.id;
    vm.value = params.question.value;
    vm.options = params.question.options;

    vm.placeholder = ko.pureComputed(function () {
        if (vm.options === undefined) {
            return;
        }

        return vm.options.placeholder;
    });

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    return { id, question };
}

export default connect(mapStateToParams, mergeParams)(fieldTextViewModel);