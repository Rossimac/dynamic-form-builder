import ko from 'knockout';
import viewModel from './step.viewmodel';
import template from './step.template.html';
import './step.styles.scss';
import '../area';

let synchronous = true;

ko.components.register('step', { template, viewModel, synchronous });
