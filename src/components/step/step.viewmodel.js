import ko from 'knockout';
import { connect } from 'knockout-store';

function stepViewModel(params) {
    const vm = this;
    vm.id = params.id;

    vm.stepSelected = ko.pureComputed(() =>
        typeof params.selectedStep() !== 'undefined' && params.selectedStep().id() === params.id()
    );
    vm.title = ko.pureComputed(() => {
        if (vm.stepSelected()) {
            return params.selectedStep().name;
        }
        return '';
    });
    vm.areas = ko.pureComputed(() => {
        if (!vm.stepSelected() || params.selectedStep().items === undefined) {
            return [];
        }

        return params.selectedStep().items();
    });

    return vm;
}

function mapStateToParams({ selectedStep, rulesEngine, visibleKeys }) {
    return { selectedStep, rulesEngine, visibleKeys };
}

function mergeParams({ selectedStep, rulesEngine, visibleKeys }, { id }) {
    return {
        selectedStep,
        id,
    };
}

export default connect(mapStateToParams, mergeParams)(stepViewModel);
