import ko from 'knockout';
import template from './debug.template.html';
import viewModel from './debug.viewmodel.js';
import './debug.styles.scss';

let synchronous = true;

ko.components.register('debug', { template, viewModel, synchronous });
