import ko from 'knockout';
import { connect } from 'knockout-store';
import { v4 as uuid } from 'uuid';
import { DateTime } from 'luxon';
import traverseLayout from '../../functions/layoutTaverser';
import populateRisk from '../../data/car/populateRisk.json';

function debugViewModel(params) {
    const vm = this;

    vm.stateItems = params.items;

    vm.debugEnabled = ko.observable(true);

    function populateRiskClicked() {
        populateRisk.items.forEach(element => {
            processElement(element);
        });
    }
    vm.populateRisk = populateRiskClicked;

    document.addEventListener('keydown', function (event) {
        if (event.ctrlKey && event.key === 'i') {
            vm.debugEnabled(!vm.debugEnabled());
        }
    });

    function processElement(element, collectionPrefix) {

        let questionId = element.key;

        if (collectionPrefix !== undefined) {
            questionId = collectionPrefix + questionId;
        }

        let question = ko.utils.arrayFirst(vm.stateItems(), function (item) {
            return item.id() === questionId;
        });

        if (question === undefined) {
            console.log(`Cound not find '${questionId}' in state`);
            return;
        }

        if (question.saved !== undefined) {
            question.saved(true);
        }

        if (element.collection !== undefined) {

            element.collection.forEach((collectionElement) => {

                let latestGroupId = uuid();

                question.collection.items.forEach((value, itemIndex) => {
                    traverseLayout(vm.stateItems, value, itemIndex, question.key, latestGroupId);
                });

                collectionElement.items.forEach(element => {
                    processElement(element, latestGroupId);
                });

                question.collection.latestGroupId(latestGroupId);

                question.collection.showDataCapture(false);
                question.collection.addAnother(false);
            });
        }

        if (element.value !== undefined) {
            question.value(element.value);
            return;
        }

        let dateValue = DateTime.now();

        if (element.computedDays) {
            dateValue = dateValue.plus({ days: element.computedDays });
        }

        if (element.computedMonths) {
            dateValue = dateValue.plus({ months: element.computedMonths });
        }

        if (element.computedYears) {
            dateValue = dateValue.plus({ years: element.computedYears });
        }

        question.value(dateValue);
    }

    return vm;
}

function mapStateToParams({ items }) {

    return { items };
}

export default connect(mapStateToParams)(debugViewModel);
