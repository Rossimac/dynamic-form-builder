import ko from 'knockout';
import template from './app.template.html';
import viewModel from './app.viewmodel.js';
import './app.variables.scss';
import './app.styles.scss';
import '../debug';
import '../step-breadcrumb';
import '../form-layout';
import '../step-nav-button';
import '../footer';
import '../header';
import 'bootstrap';
import '@fortawesome/fontawesome-free/scss/fontawesome.scss';
import '@fortawesome/fontawesome-free/scss/solid.scss'
import '@fortawesome/fontawesome-free/scss/brands.scss'
import '@fortawesome/fontawesome-free/scss/regular.scss'

let synchronous = true;

ko.components.register('app', { template, viewModel, synchronous });
