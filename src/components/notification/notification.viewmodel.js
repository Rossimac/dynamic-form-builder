import ko from 'knockout';
import { connect } from 'knockout-store';

function helpTextViewModel(params) {
    const vm = this;
    vm.items = params.items;
    vm.visibleKeys = params.visibleKeys;
    vm.notification = params.notification;

    vm.title = params.notification.titleText;
    vm.body = params.notification.bodyText;

    if (params.notification.visibility !== undefined) {
        vm.visibleKeys = params.visibleKeys;

        params.rulesEngine.addRule({
            conditions: params.notification.visibility,
            event: {
                type: 'notification-visible',
                params: {
                    key: params.notification.titleText,
                    message: params.notification.titleText
                }
            }
        })

        function findVal(object, key) {
            let foundFacts = new Set();
            Object.keys(object).forEach(function (k) {
                if (k === key) {
                    if (!foundFacts.has(object[k])) {
                        foundFacts.add(object[k]);
                    }
                    return;
                }
                if (object[k] && typeof object[k] === 'object') {
                    const nestedFacts = findVal(object[k], key);
                    nestedFacts.forEach(function (nestedFact) {
                        if (!foundFacts.has(nestedFact)) {
                            foundFacts.add(nestedFact);
                        }
                    });
                    return;
                }
            });
            return foundFacts;
        }

        const allFacts = findVal(params.notification.visibility, 'fact');

        //console.log(allFacts);

        allFacts.forEach(function (key) {
            const factState = ko.utils.arrayFirst(vm.items(), function (item) {
                return item.key === key;
            });

            params.rulesEngine.addFact(key, function (params, almanac) {
                //console.log('evaluating ' + key +' : ' + factState.key + ' is ' + factState.value())
                return factState.value();
            });
        });
    }

    vm.isVisible = ko.pureComputed(() => {
        if ( vm.visibleKeys() === undefined) {
            return false;
        }
        
        return vm.notification.visibility === undefined || vm.visibleKeys.indexOf(vm.notification.titleText) >= 0;
    });

    return vm;
}

function mapStateToParams({ rulesEngine, items, visibleKeys }) {
    return { rulesEngine, items, visibleKeys };
}

function mergeParams({ rulesEngine, items, visibleKeys }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    if(question === undefined || question.options === undefined || question.options.notification === undefined){
        console.log('Error loading notification for id ' + id);
    }

    return {
        rulesEngine: rulesEngine,
        visibleKeys: visibleKeys,
        items: items,
        notification: question.options.notification
    };
}

export default connect(mapStateToParams, mergeParams)(helpTextViewModel);
