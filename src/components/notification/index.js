import ko from 'knockout';
import template from './notification.template.html';
import viewModel from './notification.viewmodel.js';
import './notification.styles.scss';

let synchronous = true;

ko.components.register('notification', { template, viewModel, synchronous });