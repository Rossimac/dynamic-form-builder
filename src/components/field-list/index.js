import ko from 'knockout';
import template from './field-list.template.html';
import viewModel from './field-list.viewmodel.js';
import './field-list.styles.scss';

let synchronous = true;

ko.components.register('field-list', { template, viewModel, synchronous });
