import ko from 'knockout';
import { connect } from 'knockout-store';

function fieldListViewModel(params) {
    const vm = this;

    vm.value = params.question.value;
    vm.caption = params.question.options.caption;
    vm.options = params.options;

    vm.setOptionDisable = function(option, item) {
        if(item !== undefined)
        {
            return;
        }

        ko.applyBindingsToNode(option, {disable: true, hidden: true}, item);
    }
    
    return vm;
}

function mapStateToParams({ optionsDefinitions, optionsLists, items }) {
    return { optionsDefinitions, optionsLists, items };
}

function mergeParams({ optionsDefinitions, optionsLists, items }, { id, optionsKey }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    const optionsList = ko.utils.arrayFirst(optionsLists(), function (item) {
        return item.key === optionsKey;
    });

    const optionsDefinition = ko.utils.arrayFirst(optionsDefinitions(), function (item) {
        return item.key === optionsList.sourceList;
    });

    const options = optionsDefinition.options.reduce((c, n) => {
        if (optionsList.options.some(function (item) {
            return item === n.value;
        })) {
            c.push(n);
            return c;
        }
        return c;
    }, []);

    return { question, options };
}

export default connect(mapStateToParams, mergeParams)(fieldListViewModel);
