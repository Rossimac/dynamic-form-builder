import ko from 'knockout';
import template from './step-progress.template.html';
import viewModel from './step-progress.viewmodel.js';
import './step-progress.styles.scss';

let synchronous = true;

ko.components.register('step-progress', { template, viewModel, synchronous });
