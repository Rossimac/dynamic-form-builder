import ko from 'knockout';
import { connect } from 'knockout-store';

function stepProgressViewModel(params) {
    const vm = this;

    vm.step = params.step;
    vm.selectedStep = params.selectedStep;

    function getVerticalScrollPercentage(elm) {

        if (vm.selectedStep() === undefined || vm.step.ordinal() > vm.selectedStep().ordinal()) {
            return 0;
        }

        if (vm.step.ordinal() < vm.selectedStep().ordinal()) {
            return 100;
        }

        var p = elm.parentNode;
        return Math.ceil((elm.scrollTop || p.scrollTop) / (p.scrollHeight - p.clientHeight) * 100);
    }

    vm.scrollPercentage = ko.observable(getVerticalScrollPercentage(document.body));
    
    vm.effects = function () {
        vm.scrollPercentage(getVerticalScrollPercentage(document.body));
    };

    window.addEventListener('scroll', vm.effects);

    return vm;
}

function mapStateToParams({ selectedStep, items }) {

    return { selectedStep, items };
}

function mergeParams({ selectedStep, items }, { stepId }) {

    const step = ko.utils.arrayFirst(items(), function (item) {
        return item.id === stepId;
    });

    return { selectedStep, step };
}

export default connect(mapStateToParams, mergeParams)(stepProgressViewModel);