import ko from 'knockout';
import viewModel from './collection-wrapper.viewmodel';
import template from './collection-wrapper.template.html';
import './collection-wrapper.styles.scss';
import '../collection-data-capture';

let synchronous = true;

ko.components.register('collection-wrapper', { viewModel, template, synchronous });
