import ko from 'knockout';
import { connect } from 'knockout-store';

function collectionWrapperViewModel(params) {
    const vm = this;

    vm.items = params.items;
    vm.collectionQuestion = params.collectionQuestion;
    vm.value = params.collectionQuestion.value;

    vm.collectionSummaryQuestionId = "collectionSummary-" + vm.collectionQuestion.key;

    vm.groupRemovalWarning = vm.collectionQuestion.collection.options.groupRemovalWarningText;

    vm.savedData = ko.pureComputed(() => {

        let previousDataIds = [];
        ko.utils.arrayForEach(vm.items(), function (element) {
            if (element.groupKey === vm.collectionQuestion.key && previousDataIds.indexOf(element.groupId) === -1 && element.saved() === true) {
                previousDataIds.push(element.groupId)
            }
        });

        return previousDataIds;
    });

    vm.previousData = ko.pureComputed(() => {

        let previousDataIds = [];
        ko.utils.arrayForEach(vm.items(), function (element) {
            if (element.groupKey === vm.collectionQuestion.key && previousDataIds.indexOf(element.groupId) === -1 && element.saved() === true && element.removed() !== true) {
                previousDataIds.push(element.groupId)
            }
        });

        return previousDataIds;
    });

    vm.currentData = ko.pureComputed(() =>
        vm.items().filter(element =>
            stringStartsWith(element.id(), vm.collectionQuestion.collection.latestGroupId())
        )
    );

    vm.savedAndNotRemovedDataExists = ko.pureComputed(() => vm.previousData().length > 0);
    vm.onlyRemovedDataExists = ko.pureComputed(() => vm.previousData().length === 0 && vm.currentData().length > 0);

    var stringStartsWith = function (string, startsWith) {
        if (startsWith === undefined) {
            return false;
        }
        string = string || "";
        if (startsWith.length > string.length)
            return false;
        return string.substring(0, startsWith.length) === startsWith;
    };

    vm.showDataCapture = ko.pureComputed(() => {
        return vm.value() === true &&
            ((vm.collectionQuestion.collection.addAnother() === true ||
                vm.collectionQuestion.collection.showDataCapture() === true) ||
                (vm.currentData().length > 0 && vm.currentData().every((element) => element.saved() === false)))
            ||
            vm.value() === false && vm.collectionQuestion.collection.addAnother() === true && vm.savedData().length > 0;
    });

    vm.showAddButton = ko.pureComputed(() => {
        return vm.savedData().length > 0 && vm.savedData().length < vm.collectionQuestion.collection.options.max && vm.showDataCapture() === false
    });

    vm.addAnotherText = vm.collectionQuestion.collection.options.addAnotherText;

    function addAnother() {
        vm.collectionQuestion.collection.addAnother(true)
    }
    vm.addAnotherClicked = addAnother;

    vm.showRemovalWarning = ko.pureComputed(() =>
        vm.value() === false && vm.savedAndNotRemovedDataExists() === true
    );
    function removeAll() {

        vm.items.remove(function (item) {
            return item.groupKey == vm.collectionQuestion.key;
        });

        vm.collectionQuestion.collection.showDataCapture(false);
        vm.collectionQuestion.collection.addAnother(false);
    }
    vm.removeAllClicked = removeAll;

    function cancelRemoveAll() {
        vm.value(true);
    }
    vm.cancelRemoveAllClicked = cancelRemoveAll;

    if (vm.value() === true && vm.savedData().length === 0) {
        vm.collectionQuestion.collection.showDataCapture(true);
    } else {
        vm.collectionQuestion.collection.showDataCapture(false);
    }
    vm.collectionQuestion.collection.addAnother(false);

    vm.opacityValue = ko.pureComputed(() => vm.showRemovalWarning() ? "opacity-25" : "opacity-100");

    vm.yesSubscription = vm.value.subscribe((value) => {
        if (value === true && vm.savedData().length === 0) {
            vm.value.isModified(false);
            vm.collectionQuestion.collection.addAnother(true);
        }else if(value === false && vm.savedAndNotRemovedDataExists() === false){
            removeAll();
        }
    });

    vm.onlyRemovedDataExists.subscribe((newValue) => {
        if(newValue === true)
        {
            vm.value.isModified(false);
        }
    });

    vm.collectionQuestion.value.rules.remove(function (item) {
        return item.message === vm.collectionQuestion.collection.options.noSavedItemsErrorMessage;
    });

    vm.collectionQuestion.value.extend({
        validation: {
            validator: (collectionIsYes, { savedAndNotRemovedDataExists, dataCaptureShown }) => {
               
                return collectionIsYes === false || savedAndNotRemovedDataExists() === true || dataCaptureShown() === true;
            },
            message: vm.collectionQuestion.collection.options.noSavedItemsErrorMessage,
            params: { savedAndNotRemovedDataExists: vm.savedAndNotRemovedDataExists, dataCaptureShown: vm.collectionQuestion.collection.showDataCapture }
        }
    });

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    const collectionQuestion = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    return { collectionQuestion, items };
}

collectionWrapperViewModel.prototype.dispose = function () {
    const vm = this;

    vm.items.remove(function (item) {
        return item.groupKey == vm.collectionQuestion.key && item.saved() === false;
    });

    vm.yesSubscription.dispose();

    if (vm.value() === false && vm.savedAndNotRemovedDataExists().length > 0) {
        vm.value(true);
    }
}

export default connect(mapStateToParams, mergeParams)(collectionWrapperViewModel);
