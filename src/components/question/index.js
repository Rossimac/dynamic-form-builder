import ko from 'knockout';
import viewModel from './question.viewmodel';
import template from './question.template.html';
import './question.styles.scss';
import '../collection-summary';
import '../collection-wrapper';
import '../field-assumption-list';
import '../field-checkbox';
import '../field-currency';
import '../field-date';
import '../field-date-single-format';
import '../field-label';
import '../field-list';
import '../field-number';
import '../field-number-select';
import '../field-radio';
import '../field-text';
import '../help-text';
import '../notification';
import '../vehicle-find';
import '../vehicle-summary';

let synchronous = true;

ko.components.register('question', { template, viewModel, synchronous });