import ko from 'knockout';
import { connect } from 'knockout-store';
import { DateTime } from 'luxon';

function questionViewModel(params) {
    const vm = this;

    vm.id = params.id;
    vm.isDependantQuestion = params.isDependantQuestion;
    vm.key = params.question.key;
    vm.type = params.question.type;
    vm.value = params.question.value;
    vm.mappedQuestions = params.mappedQuestions;
    vm.resetQuestionsOnAnswer = params.resetQuestionsOnAnswer;
    vm.validationItems = params.validationItems;

    // if (vm.value === undefined) {
    //     vm.value = ko.observable();//hack to stop validation stuff failing
    // }

    vm.question = params.question;
    vm.visibleKeys = params.visibleKeys;
    vm.items = params.items;

    vm.previouslyVisible = false;
    vm.firstRun = true;

    if (params.question.visibility !== undefined) {
        vm.visibleKeys = params.visibleKeys;

        params.rulesEngine.addRule({
            conditions: params.question.visibility,
            event: {
                type: 'question-visible',
                params: {
                    key: params.question.key,
                    message: params.question.key
                }
            }
        })

        function findVal(object, key) {
            let foundFacts = new Set();
            Object.keys(object).forEach(function (k) {
                if (k === key) {
                    if (!foundFacts.has(object[k])) {
                        foundFacts.add(object[k]);
                    }
                    return;
                }
                if (object[k] && typeof object[k] === 'object') {
                    const nestedFacts = findVal(object[k], key);
                    nestedFacts.forEach(function (nestedFact) {
                        if (!foundFacts.has(nestedFact)) {
                            foundFacts.add(nestedFact);
                        }
                    });
                    return;
                }
            });
            return foundFacts;
        }

        const allFacts = findVal(params.question.visibility, 'fact');

        //console.log(allFacts);

        allFacts.forEach(function (key) {
            const factState = ko.utils.arrayFirst(vm.items(), function (item) {
                return item.key === key;
            });

            params.rulesEngine.addFact(key, function (params, almanac) {
                //console.log('evaluating ' + key + ' : ' + factState.key + ' is ' + factState.value())
                return factState.value();
            });
        });
    }

    if (params.question.options !== undefined && params.question.options.smallText !== undefined) {
        vm.smallText = params.question.options.smallText.text;

        if (params.question.options.smallText.visibility !== undefined) {
            vm.visibleKeys = params.visibleKeys;

            params.rulesEngine.addRule({
                conditions: params.question.options.smallText.visibility,
                event: {
                    type: 'small-text-visible',
                    params: {
                        key: params.question.options.smallText.text,
                        message: params.question.options.smallText.text// + ' visible because ' + params.visibilityItem.key + ' ' + params.question.visibility.operator + "'" + params.question.visibility.value + "'"
                    }
                }
            })

            function findVal(object, key) {
                let foundFacts = new Set();
                Object.keys(object).forEach(function (k) {
                    if (k === key) {
                        if (!foundFacts.has(object[k])) {
                            foundFacts.add(object[k]);
                        }
                        return;
                    }
                    if (object[k] && typeof object[k] === 'object') {
                        const nestedFacts = findVal(object[k], key);
                        nestedFacts.forEach(function (nestedFact) {
                            if (!foundFacts.has(nestedFact)) {
                                foundFacts.add(nestedFact);
                            }
                        });
                        return;
                    }
                });
                return foundFacts;
            }

            const allFacts = findVal(params.question.options.smallText.visibility, 'fact');

            //console.log(allFacts);

            allFacts.forEach(function (key) {
                const factState = ko.utils.arrayFirst(vm.items(), function (item) {
                    return item.key === key;
                });

                params.rulesEngine.addFact(key, function (params, almanac) {
                    //console.log('evaluating ' + key +' : ' + factState.key + ' is ' + factState.value())
                    return factState.value();
                });
            });
        }
    }

    vm.questionVisible = ko.computed(() => {

        if (vm.question.visibility === undefined || vm.visibleKeys() === undefined) {
            return true;
        }

        const nowVisible = vm.visibleKeys.indexOf(vm.key) >= 0;

        if (params.question.value !== undefined && !(vm.firstRun === true && params.question.value() !== undefined) && (nowVisible === false || (vm.previouslyVisible === false && nowVisible === true))) {

            if (nowVisible && !vm.previouslyVisible && params.question.value.isModified !== undefined) {
                params.question.value.isModified(false);
            }

            // if (vm.question.options.clearWhenInvisible !== false) {
            //     if (nowVisible && params.question.options !== undefined && params.question.value() === undefined) {
            //         params.question.value(params.question.options.defaultValue);
            //     }
            //     else {
            //         params.question.value(undefined);
            //     }
            // }
        }

        vm.previouslyVisible = nowVisible;
        vm.firstRun = false;

        return nowVisible;
    });

    if (params.question.value !== undefined) {
        vm.rulesEngineRunner = vm.value.subscribe(function () {

            params.rulesEngine
                .run()
                .then(({ events }) => {
                    const visibleKeys = events.map(a => a.params.key);
                    vm.visibleKeys(visibleKeys);
                })
        });
    }

    if (params.resetQuestionsOnAnswer.length > 0) {
        vm.resetQuestionsOnAnswerSubscription = vm.value.subscribe(function () {
            params.resetQuestionsOnAnswer.forEach(element => {
                element(undefined);
                element.isModified(false);
            });
        });
    }

    vm.validationConfig = ko.computed(() => {

        if (params.question.validation !== undefined) {

            if (vm.questionVisible() === false) {
                return;
            }

            params.question.validation.forEach(ConfigureValidation);
        }
    });

    function ConfigureValidation(validation) {

        if (params.question.value.rules !== undefined) {
            let existingValidation = ko.utils.arrayFirst(params.question.value.rules(), function (item) {
                return item.rule == validation.type;
            });

            if (existingValidation !== undefined) {
                return;
            }
        }

        if (validation.type === 'required') {
            ConfigureValidationRequired(validation)
        }

        if (validation.type === 'min') {
            ConfigureValidationMin(validation)
        }

        if (validation.type === 'max') {
            ConfigureValidationMax(validation)
        }

        if (validation.type === 'minLength') {
            ConfigureValidationMinLength(validation)
        }

        if (validation.type === 'maxLength') {
            ConfigureValidationMaxLength(validation)
        }

        if (validation.basedOn) {
            vm.basedOnSubscription = vm.validationItems[validation.basedOn].value.subscribe(function (newValue) {
                ConfigureBasedOnValidation(validation, newValue);
            });
            ConfigureBasedOnValidation(validation, vm.validationItems[validation.basedOn].value());
        } else {
            if (validation.type === 'dateLessThan') {
                ConfigureValidationDateLessThan(validation)
            }

            if (validation.type === 'dateLessThanOrEqualTo') {
                ConfigureValidationDateLessThanOrEqualTo(validation)
            }

            if (validation.type === 'dateGreaterThan') {
                ConfigureValidationDateGreaterThan(validation)
            }

            if (validation.type === 'dateGreaterThanOrEqualTo') {
                ConfigureValidationDateGreaterThanOrEqualTo(validation)
            }

            if (validation.type === 'dateNotBetween') {
                ConfigureValidationDateNotBetween(validation)
            }
        }
    }

    function ConfigureBasedOnValidation(validation, newValue) {
        vm.value.rules.remove(function (item) {
            return item.rule === validation.type && item.message === validation.message;
        });

        if (validation.type === 'dateLessThan') {
            ConfigureValidationDateLessThan(validation, newValue);
        }

        if (validation.type === 'dateLessThanOrEqualTo') {
            ConfigureValidationDateLessThanOrEqualTo(validation, newValue);
        }

        if (validation.type === 'dateGreaterThan') {
            ConfigureValidationDateGreaterThan(validation, newValue);
        }

        if (validation.type === 'dateGreaterThanOrEqualTo') {
            ConfigureValidationDateGreaterThanOrEqualTo(validation, newValue);
        }
    }

    function ConfigureValidationRequired(validation) {

        if (validation.required === true) {
            params.question.value.extend({
                required: {
                    message: validation.message,
                },
            });
        }

        if (validation.onlyIf !== undefined) {
            validation.onlyIf.all.forEach((rule) => {

                if (rule.operator === 'equal') {
                    params.question.value.extend({
                        required: {
                            message: validation.message,
                            onlyIf: function () {
                                return vm.validationItems[rule.fact].value() === rule.value;
                            }
                        }
                    });
                }

                if (rule.operator === 'notEqual') {
                    params.question.value.extend({
                        required: {
                            message: validation.message,
                            onlyIf: function () {
                                return vm.validationItems[rule.fact].value() !== rule.value;
                            }
                        }
                    });
                }
            });
        }
    }

    function ConfigureValidationMin(validation) {
        params.question.value.extend({
            min: {
                params: validation.min,
                message: validation.message,
            },
        });
    }

    function ConfigureValidationMax(validation) {
        params.question.value.extend({
            max: {
                params: validation.max,
                message: validation.message,
            },
        });
    }

    function ConfigureValidationMinLength(validation) {
        params.question.value.extend({
            minLength: {
                params: validation.min,
                message: validation.message,
            },
        });
    }

    function ConfigureValidationMaxLength(validation) {
        params.question.value.extend({
            maxLength: {
                params: validation.max,
                message: validation.message,
            },
        });
    }

    function ConfigureValidationDateLessThan(validation, basedOnDate) {

        let validationDate;
        
        if(basedOnDate){
            validationDate = DateTime.fromISO(basedOnDate);
        }else{
            validationDate = DateTime.now();
        }

        validationDate = validationDate.startOf('day');

        if (validation.days) {
            validationDate = validationDate.plus({ days: validation.days });
        }

        if (validation.months) {
            validationDate = validationDate.plus({ months: validation.months });
        }

        if (validation.years) {
            validationDate = validationDate.plus({ years: validation.years });
        }

        params.question.value.extend({
            dateLessThan: {
                params: validationDate.toISO(),
                message: validation.message,
            },
        });
    }

    function ConfigureValidationDateLessThanOrEqualTo(validation, basedOnDate) {

        let validationDate;
        
        if(basedOnDate){
            validationDate = DateTime.fromISO(basedOnDate);
        }else{
            validationDate = DateTime.now();
        }

        validationDate = validationDate.startOf('day');

        if (validation.days) {
            validationDate = validationDate.plus({ days: validation.days });
        }

        if (validation.months) {
            validationDate = validationDate.plus({ months: validation.months });
        }

        if (validation.years) {
            validationDate = validationDate.plus({ years: validation.years });
        }

        params.question.value.extend({
            dateLessThanOrEqualTo: {
                params: validationDate.toISO(),
                message: validation.message,
            },
        });
    }

    function ConfigureValidationDateGreaterThan(validation, basedOnDate) {

        let validationDate;
        
        if(basedOnDate){
            validationDate = DateTime.fromISO(basedOnDate);
        }else{
            validationDate = DateTime.now();
        }

        validationDate = validationDate.startOf('day');

        if (validation.days) {
            validationDate = validationDate.plus({ days: validation.days });
        }

        if (validation.months) {
            validationDate = validationDate.plus({ months: validation.months });
        }

        if (validation.years) {
            validationDate = validationDate.plus({ years: validation.years });
        }

        params.question.value.extend({
            dateGreaterThan: {
                params: validationDate.toISO(),
                message: validation.message,
            },
        });
    }

    function ConfigureValidationDateGreaterThanOrEqualTo(validation, basedOnDate) {

        let validationDate;
        
        if(basedOnDate){
            validationDate = DateTime.fromISO(basedOnDate);
        }else{
            validationDate = DateTime.now();
        }

        validationDate = validationDate.startOf('day');

        if (validation.days) {
            validationDate = validationDate.plus({ days: validation.days });
        }

        if (validation.months) {
            validationDate = validationDate.plus({ months: validation.months });
        }

        if (validation.years) {
            validationDate = validationDate.plus({ years: validation.years });
        }

        params.question.value.extend({
            dateGreaterThanOrEqualTo: {
                params: validationDate.toISO(),
                message: validation.message,
            },
        });
    }

    function ConfigureValidationDateNotBetween(validation) {

        let lowValidationDate = DateTime.now().startOf('day');

        if (validation.lowDays) {
            lowValidationDate = lowValidationDate.plus({ days: validation.lowDays });
        }

        if (validation.lowMonths) {
            lowValidationDate = lowValidationDate.plus({ months: validation.lowMonths });
        }

        if (validation.lowYears) {
            lowValidationDate = lowValidationDate.plus({ years: validation.lowYears });
        }

        let highValidationDate = DateTime.now().startOf('day');

        if (validation.highDays) {
            highValidationDate = highValidationDate.plus({ days: validation.highDays });
        }

        if (validation.highMonths) {
            highValidationDate = highValidationDate.plus({ months: validation.highMonths });
        }

        if (validation.highYears) {
            highValidationDate = highValidationDate.plus({ years: validation.highYears });
        }

        params.question.value.extend({
            dateNotBetween: {
                params: { lowValidationDate: lowValidationDate.toISO(), highValidationDate: highValidationDate.toISO() },
                message: validation.message,
            },
        });
    }

    vm.smallTextVisible = ko.pureComputed(() => {
        if (params.question.options === undefined || vm.question.options.smallText === undefined || vm.visibleKeys() === undefined) {
            return false;
        }

        return vm.question.options.smallText.visibility === undefined || vm.visibleKeys.indexOf(vm.question.options.smallText.text) >= 0;
    });

    vm.smallText = ko.pureComputed(() => {
        if (vm.smallTextVisible()) {
            return vm.question.options.smallText.text;
        }
        return '';
    });

    vm.hasHelpText = ko.pureComputed(() => {
        return params.question.options !== undefined && params.question.options.helpText !== undefined;
    });

    vm.hasNotification = ko.pureComputed(() => {
        return params.question.options !== undefined && params.question.options.notification !== undefined;
    });

    vm.hasItems = ko.pureComputed(() => {
        return params.question.items !== undefined;
    });

    vm.items = ko.pureComputed(() => {
        if (vm.hasItems()) {
            return params.question.items;
        }
        return;
    });

    vm.showCollection = ko.pureComputed(() => {
        return params.question.collection !== undefined;
    });

    return vm;
}

questionViewModel.prototype.dispose = function () {

    const vm = this;

    vm.questionVisible.dispose();
    vm.validationConfig.dispose();
    vm.rulesEngineRunner.dispose();
    if (vm.resetQuestionsOnAnswerSubscription !== undefined) {
        vm.resetQuestionsOnAnswerSubscription.dispose();
    }
    // if (vm.basedOnSubscription !== undefined) {
    //     vm.basedOnSubscription.dispose();
    // }
};

function mapStateToParams({ rulesEngine, items, visibleKeys }) {
    return { rulesEngine, items, visibleKeys };
}

function mergeParams({ rulesEngine, items, visibleKeys }, { id, isDependantQuestion }) {

    let question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    // if (question === undefined) {
    //     question = ko.utils.arrayFirst(items(), function (item) {
    //         return item.id() === id;
    //     });
    // }

    const validationItems = {};

    if (question.validation !== undefined) {

        question.validation.forEach((validationRule) => {
            if (validationRule.onlyIf !== undefined) {
                validationRule.onlyIf.all.forEach((rule) => {
                    validationItems[rule.fact] = ko.utils.arrayFirst(items(), function (item) {
                        return item.key === rule.fact;
                    });
                });
            }

            if (validationRule.basedOn !== undefined) {
                validationItems[validationRule.basedOn] = ko.utils.arrayFirst(items(), function (item) {
                    return item.key === validationRule.basedOn;
                });
            }
        });
    }

    let mappedQuestions = [];

    if (question.options !== undefined && question.options.mappedQuestions !== undefined) {
        mappedQuestions = items().reduce((c, n) => {
            if (question.options.mappedQuestions.some(function (item) {
                return item.to === n.key;
            })) {
                c.push({ key: n.key, value: n.value });
                return c;
            }
            return c;
        }, []);
    }


    let resetQuestionsOnAnswer = [];

    if (question.options?.resetQuestionsOnAnswer !== undefined) {
        resetQuestionsOnAnswer = items().reduce((c, n) => {
            if (question.options.resetQuestionsOnAnswer.some(function (item) {
                return item === n.key;
            })) {
                c.push(n.value);
                return c;
            }
            return c;
        }, []);
    }

    return { rulesEngine, question, id, isDependantQuestion, validationItems, items, visibleKeys, mappedQuestions, resetQuestionsOnAnswer };
}

export default connect(mapStateToParams, mergeParams)(questionViewModel);
