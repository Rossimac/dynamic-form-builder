import ko from 'knockout';
import template from './field-text.template.html';
import viewModel from './field-text.viewmodel.js';
import './field-text.styles.scss';

let synchronous = true;

ko.components.register('field-text', { template, viewModel, synchronous });