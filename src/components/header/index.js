import ko from 'knockout';
import template from './header.template.html';
import viewModel from './header.viewmodel.js';
import './header.styles.scss';

let synchronous = true;

ko.components.register('header-component', { template, viewModel, synchronous });
