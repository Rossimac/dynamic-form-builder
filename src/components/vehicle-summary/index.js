import ko from 'knockout';
import template from './vehicle-summary.template.html';
import viewModel from './vehicle-summary.viewmodel.js';
import './vehicle-summary.styles.scss';

let synchronous = true;

ko.components.register('vehicle-summary', { template, viewModel, synchronous });