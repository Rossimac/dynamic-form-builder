import ko from 'knockout';
import { connect } from 'knockout-store';

function vehicleSummaryViewModel(params) {
    const vm = this;

    vm.resetQuestions = params.resetQuestions;
    vm.registration = params.registration.value;
    vm.vehicleMake = params.vehicleMake.value;
    vm.vehicleModel = params.vehicleModel.value;
    vm.detailsCorrect = params.detailsCorrect.value;

    vm.vehicleSummary = ko.pureComputed(() => {
        let result = vm.vehicleMake();

        if (vm.registration() !== undefined) {
            result += ' (' + vm.registration() + ')'
        }

        return result;
    });

    function notTheRightCar() {

        if (vm.detailsCorrect() !== undefined) {
            vm.detailsCorrect(undefined);
            if (vm.detailsCorrect.isModified !== undefined) {
                vm.detailsCorrect.isModified(false);
            }
        }

        vm.resetQuestions.forEach(resetQuestion => {

            if (resetQuestion.value() !== undefined) {
                resetQuestion.value(undefined);

                if (resetQuestion.value.isModified !== undefined) {
                    resetQuestion.value.isModified(false);
                }
                return;
            }
        });
    }
    vm.notTheRightCarClicked = notTheRightCar;

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    const registration = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === "Registration";
    });

    const vehicleMake = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === "VehicleMake";
    });

    const vehicleModel = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === "VehicleModel";
    });

    const detailsCorrect = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === "DetailsCorrect";
    });

    let resetQuestions = [];

    if (question.options !== undefined && question.options.resetQuestions !== undefined) {
        resetQuestions = items().reduce((c, n) => {
            var resetQuestionsForItem = question.options.resetQuestions.filter(function (item) {
                return item === n.key;
            });

            resetQuestionsForItem.forEach(resetQuestionForItem => {
                c.push({ key: resetQuestionForItem, value: n.value });
            });

            return c;
        }, []);
    }

    return {
        resetQuestions: resetQuestions,
        registration: registration,
        vehicleMake: vehicleMake,
        vehicleModel: vehicleModel,
        detailsCorrect: detailsCorrect
    };
}

export default connect(mapStateToParams, mergeParams)(vehicleSummaryViewModel);
