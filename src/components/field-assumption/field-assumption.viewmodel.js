import ko from 'knockout';
import { connect } from 'knockout-store';

function fieldListViewModel(params) {
    const vm = this;

    vm.visibleKeys = params.visibleKeys;
    vm.items = params.items;

    vm.value = params.question.value;

    vm.assumptionTexts = ko.observableArray();

    if (params.question.options?.assumptionTexts === undefined) {
        return;
    }

    params.question.options.assumptionTexts.forEach((assumptionText, index) => {

        const text = ko.pureComputed(function () {
            let mergedText = assumptionText.text;

            if (mergedText !== undefined) {
                return mergedText;
            }

            mergedText = assumptionText.template;

            assumptionText.mergeVariables.forEach((mergeVariable, mergeVariableIndex) => {

                const mergeVariableQuestion = ko.utils.arrayFirst(vm.items(), function (item) {
                    return item.id() === mergeVariable;
                });

                mergedText = mergedText.replace("{" + mergeVariableIndex + "}", mergeVariableQuestion.value())
            });

            return mergedText;
        });

        const ruleKey = 'assumption-' + index + '-' + params.question.key;

        if (assumptionText.visibility !== undefined) {

            params.rulesEngine.addRule({
                conditions: assumptionText.visibility,
                event: {
                    type: 'assumption-visible',
                    params: {
                        key: ruleKey,
                        message: ruleKey// + ' visible because ' + params.visibilityItem.key + ' ' + params.question.visibility.operator + "'" + params.question.visibility.value + "'"
                    }
                }
            })

            function findVal(object, key) {
                let foundFacts = new Set();
                Object.keys(object).forEach(function (k) {
                    if (k === key) {
                        if (!foundFacts.has(object[k])) {
                            foundFacts.add(object[k]);
                        }
                        return;
                    }
                    if (object[k] && typeof object[k] === 'object') {
                        const nestedFacts = findVal(object[k], key);
                        nestedFacts.forEach(function (nestedFact) {
                            if (!foundFacts.has(nestedFact)) {
                                foundFacts.add(nestedFact);
                            }
                        });
                        return;
                    }
                });
                return foundFacts;
            }

            const allFacts = findVal(assumptionText.visibility, 'fact');

            //console.log(allFacts);

            allFacts.forEach(function (key) {
                const factState = ko.utils.arrayFirst(vm.items(), function (item) {
                    return item.key === key;
                });

                params.rulesEngine.addFact(key, function (params, almanac) {
                    //console.log('evaluating ' + key +' : ' + factState.key + ' is ' + factState.value())
                    return factState.value();
                });
            });
        }

        const visibility = ko.pureComputed(() => {
            if (assumptionText.visibility === undefined || vm.visibleKeys() === undefined) {
                return true;
            }

            return vm.visibleKeys.indexOf(ruleKey) >= 0;
        });

        vm.assumptionTexts.push({ text, visibility });
    });

    params.rulesEngine
        .run()
        .then(({ events }) => {
            const visibleKeys = events.map(a => a.params.key);
            vm.visibleKeys(visibleKeys);
        })

    return vm;
}

function mapStateToParams({ rulesEngine, items, optionsDefinitions, visibleKeys }) {
    return { rulesEngine, items, visibleKeys };
}

function mergeParams({ rulesEngine, items, optionsDefinitions, visibleKeys }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    return { rulesEngine, visibleKeys, items, question };
}

export default connect(mapStateToParams, mergeParams)(fieldListViewModel);
