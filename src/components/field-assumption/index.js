import ko from 'knockout';
import template from './field-assumption.template.html';
import viewModel from './field-assumption.viewmodel.js';
import './field-assumption.styles.scss';

let synchronous = true;

ko.components.register('field-assumption', { template, viewModel, synchronous });
