import ko from 'knockout';
import template from './field-label.template.html';
import viewModel from './field-label.viewmodel.js';
import './field-label.styles.scss';

let synchronous = true;

ko.components.register('field-label', { template, viewModel, synchronous });