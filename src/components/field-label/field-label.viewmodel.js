import ko from 'knockout';
import { connect } from 'knockout-store';

function fieldLabelViewModel(params) {
    const vm = this;

    vm.label = params.label;
    vm.value = params.question.value;

    vm.isValid = ko.pureComputed(() => {

        return vm.value.rules === undefined ||
            (vm.value !== undefined &&
                vm.value.isModified !== undefined &&
                (vm.value.isValid === undefined ||
                    vm.value.isValid()));
    });

    return vm;
}

function mapStateToParams({ items }) {
    return { items };
}

function mergeParams({ items }, { id }) {

    const question = ko.utils.arrayFirst(items(), function (item) {
        return item.id() === id;
    });

    return {
        label: question.name,
        question: question
    };
}

export default connect(mapStateToParams, mergeParams)(fieldLabelViewModel);
