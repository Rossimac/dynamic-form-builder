import ko from 'knockout';
import { setState } from 'knockout-store';
import layout from './data/car/layout';
import optionsDefinitions from './data/car/options.definitions';
import optionsLists from './data/car/option.lists';
import './components/app';
import $ from 'jquery';
import { Router } from '@profiscience/knockout-contrib-router'
import { Engine } from 'json-rules-engine';
import traverseLayout from './functions/layoutTaverser';
import format from 'string-format';
import { DateTime } from 'luxon';

ko.deferUpdates = true;

const state = {
    optionsDefinitions: ko.observableArray([]),
    optionsLists: ko.observableArray([]),
    riskItems: ko.observableArray([]),
    items: ko.observableArray([]),
    selectedStep: ko.observable(),
    rulesEngine: new Engine(),
    visibleKeys: ko.observableArray([])
};

format.extend(String.prototype, {
    escape: s =>
        s.replace(/[&<>"'`]/g, c => '&#' + c.charCodeAt(0) + ';'),
    upper: s =>
        s.toUpperCase(),
});

setState(state);

setTimeout(() => {

    layout.forEach((value, index) => {
        traverseLayout(state.items, value, index);
    });

    state.optionsDefinitions(optionsDefinitions);
    state.optionsLists(optionsLists);

    const firstStep = ko.utils.arrayFirst(state.items(), function (item) {
        return item.type === 'step';
    });

    state.selectedStep(firstStep);

    state.rulesEngine
        .run()
        .then(({ events }) => {
            const visibleKeys = events.map(a => a.params.key);
            state.visibleKeys(visibleKeys);
        })

    setTimeout(() => {
        window.scrollTo(0, 0);

    }, 100);

}, 100);

ko.validation.init({
    decorateInputElement: false,
    errorMessageClass: 'error-block',
    errorElementClass: 'border-start border-5 border-danger',
    allowHtmlMessages: true,
    grouping: {
        deep: true,
        live: true,
        observable: true
    },
    insertMessages: false,
    messagesOnModified: true
}, true);


ko.validation.rules['dateLessThan'] = {
    validator: (val, otherVal) => DateTime.fromISO(val) <= DateTime.fromISO(otherVal)
};

ko.validation.rules['dateLessThanOrEqualTo'] = {
    validator: (val, otherVal) => DateTime.fromISO(val) < DateTime.fromISO(otherVal)
};

ko.validation.rules['dateGreaterThan'] = {
    validator: (val, otherVal) => DateTime.fromISO(val) >= DateTime.fromISO(otherVal)
};

ko.validation.rules['dateGreaterThanOrEqualTo'] = {
    validator: (val, otherVal) => DateTime.fromISO(val) > DateTime.fromISO(otherVal)
};

ko.validation.rules['dateNotBetween'] = {
    validator: (val, { lowValidationDate, highValidationDate }) => {
        var valAsISO = DateTime.fromISO(val);
        return DateTime.fromISO(lowValidationDate) > valAsISO && valAsISO <= DateTime.fromISO(highValidationDate);
    }
};

ko.validation.registerExtenders();

Router.useRoutes({
    routes: {
        '/': 'home', // component
    }
})

ko.applyBindings();
