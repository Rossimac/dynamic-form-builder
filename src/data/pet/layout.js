const layout = [
    {
        key: 'your-pet',
        type: 'step',
        name: 'Your Pet',
        items: [
            {
                key: 'about-your-pet',
                type: 'area',
                name: 'About your pet',
                intro: {
                    header: 'Let’s insure your furriest family member',
                    subHeader: `For your cover to be valid, please make sure that you are 
                    the owner of your pet and
                    living with them at the same address and
                    a full-time UK resident.`,
                    hint: `We'll pass on the following information to insurers so that we can find the right quote to suit you both.`
                },
                items: [
                    {
                        key: 'PetType',
                        type: 'radio',
                        name: 'Does your pet ...?',
                        options: {
                            optionsKey: 'petType'
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please select the type of pet you would like to insure`,
                            }
                        ]
                    },
                    {
                        key: 'PetName',
                        type: 'text',
                        name: `What's their name?`,
                        options: {
                            placeholder: 'e.g. Fido, Bella or Lady of the Manor',
                            helpText: `To make our insurance journey a little bit more special to you, we will refer to your pet's name in the questions we ask.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please enter your pet's name`,
                            },
                            {
                                type: 'maxLength',
                                max: 50,
                                message: `Oops - are you sure? This doesn't look right`
                            }
                        ]
                    },
                    {
                        key: 'PetGender',
                        type: 'radio',
                        name: 'Is your pet a...?',
                        options: {
                            optionsKey: 'gender'

                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please select your pet's gender`,
                            }
                        ]
                    },
                    {
                        key: 'PetDateOfBirth',
                        type: 'date',
                        name: `What is your pet's date of birth?`,
                        options: {
                            monthCaption: 'Month',
                            yearCaption: 'Year',
                            day: {
                                visible: false
                            },
                            month: {
                                visible: true,
                                caption: 'Month'
                            },
                            year: {
                                visible: true,
                                caption: 'Year'
                            },
                            helpText: `Just like humans, insurance costs for pets can vary due to their age. We need to know how old your pet is so that we can show you the most accurate quotes that we can. Your pet needs to be at least 5 weeks old to be covered, although some insurers' policies start from 8 weeks and over.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please enter a valid date`,
                            }
                        ]
                    },
                    {
                        key: 'DogBreed',
                        type: 'radio',
                        name: 'Is your pet a...?',
                        options: {
                            optionsKey: 'dog-breed-type',
                            helpText: `Select 'pedigree' if your dog's parents are both the same breed, or 'crossbreed' if they have parents from two different breeds. If you're not sure what breed they are (or if they're a mix of several different breeds), select 'mongrel'.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please select your pet's breed category`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'PetType',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'PetType',
                                operator: 'equal',
                                value: 'D'
                            }]
                        }
                    },
                    {
                        key: 'CatBreed',
                        type: 'radio',
                        name: 'Is your pet a...?',
                        options: {
                            optionsKey: 'cat-breed-type',
                            helpText: `Select 'pedigree' if your cat's parents are both the same breed. If you're not sure what breed they are (or if they're a mix of several different breeds), select 'moggie'.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please select your pet's breed category`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'PetType',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'PetType',
                                operator: 'equal',
                                value: 'C'
                            }]
                        }
                    },
                    {
                        key: 'PetSize',
                        type: 'radio',
                        name: `How big is your pet?`,
                        options: {
                            optionsKey: 'pet-size',
                            helpText: `Pets come in all shapes and sizes, so pop them on the scales or use their most recent weigh-in.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please select the size of your pet`,
                            }
                        ],
                        visibility: {
                            any: [{
                                fact: 'DogBreed',
                                operator: 'equal',
                                value: 'C'
                            }, {
                                fact: 'DogBreed',
                                operator: 'equal',
                                value: 'M'
                            }]
                        }
                    },
                    {
                        key: 'PetBreedName',
                        type: 'text',
                        name: `What is the name of the breed?`,
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please enter your pet's breed`,
                            },
                            {
                                type: 'maxLength',
                                max: 50,
                                message: `Oops - are you sure? This doesn't look right`
                            }
                        ],
                        visibility: {
                            any: [{
                                fact: 'DogBreed',
                                operator: 'equal',
                                value: 'P'
                            }, {
                                fact: 'DogBreed',
                                operator: 'equal',
                                value: 'C'
                            }, {
                                fact: 'CatBreed',
                                operator: 'equal',
                                value: 'P'
                            }]
                        }
                    },
                    {
                        key: 'PetValue',
                        type: 'currency',
                        name: 'How much did you pay or donate for your pet?',
                        options: {
                            helpText: `Please tell us how much you paid (or donated) for your pet to the nearest pound. Inaccurate information might limit how much is paid out in the event that your pet is lost or stolen. Some insurers require proof of how much you paid or they will only pay out a minimal sum in the event of a claim.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please enter how much you paid for your pet`,
                            },
                            {
                                type: 'min',
                                min: 1,
                                message: `Oops - are you sure? This doesn't look right`
                            },
                            {
                                type: 'max',
                                max: 9999,
                                message: `Please enter a value less than or equal to 9999.`
                            }
                        ]
                    },
                ]
            },
            {
                key: 'pet-medical-history',
                type: 'area',
                name: `Let's take a look at your pet's medical history`,
                options: {
                    notification: {
                        titleText: 'Did you know...',
                        bodyText: `Pet insurance is meant to protect against future events. This means that it won't cover Ross for any known medical conditions, illness or injuries that your pet already has (pre-existing conditions). If you want to cover anything Ross already suffers with then you'll need to speak to a specialist provider.`,
                        visibility: {
                            any: [{
                                fact: 'PreExistingHealthConditions',
                                operator: 'equal',
                                value: undefined
                            }, {
                                fact: 'PreExistingHealthConditions',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    }
                },
                items: [
                    {
                        key: 'PreExistingHealthConditions',
                        type: 'radio',
                        name: 'Does your policy need to cover your pet for any pre-existing medical conditions?',
                        options: {
                            optionsKey: 'yes-no',
                            helpText: `What is a pre-existing condition?
                            Specific definitions might vary between insurers, but most categorise a pre-existing condition as:
                            
                            A condition that occurred or showed symptoms before the insurance policy was taken out.
                            A condition that results in the same diagnosis that your pet had before the policy was bought.
                            A condition resulting from an illness or injury your pet had before you took the policy out.`, 
                            notification: {
                                bodyText: `Insurance for pre-existing conditions needs to be sorted out by a specialist provider, which we can't offer at the moment. If you need information about pre-existing conditions and how to arrange cover, please see our specialist provider guide here.`,
                                visibility: {
                                    all: [{
                                        fact: 'PreExistingHealthConditions',
                                        operator: 'notEqual',
                                        value: undefined
                                    }, {
                                        fact: 'PreExistingHealthConditions',
                                        operator: 'equal',
                                        value: true
                                    }]
                                }
                            }
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please select whether your pet has had any pre-existing conditions which you want to cover under this policy`,
                            }
                        ]
                    },
                    {
                        key: 'NeuteredOrSpayed',
                        type: 'radio',
                        name: `Has your pet been neutered / spayed?`,
                        options: {
                            optionsKey: 'yes-no',
                            helpText: `Neutering / spaying is the process of removing your pet's ability to reproduce. This procedure is carried out by a vet, usually when the animal is young.`
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please select whether your pet has been neutered or spayed`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'PreExistingHealthConditions',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'PreExistingHealthConditions',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'Chipped',
                        type: 'radio',
                        name: `Has your pet been chipped?`,
                        options: {
                            optionsKey: 'yes-no',
                            helpText: `A microchip is a small implant placed under the skin of an animal and contains a unique identification number. Microchips are commonly used to help identify pets if they become lost or stolen.`
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please select whether your pet has been chipped`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'PreExistingHealthConditions',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'PreExistingHealthConditions',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'Vaccinated',
                        type: 'radio',
                        name: `Are your pet's vaccines up to date?`,
                        options: {
                            optionsKey: 'yes-no',
                            helpText: `Although pets do not need to be vaccinated in order to be insured, if your pet happens to suffer from an illness which could have been prevented by the administration of a "routine" vaccine then the insurer may refuse to pay the claim. It's important to read the terms and conditions carefully.`
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please confirm whether your pet's vaccines are up to date`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'PreExistingHealthConditions',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'PreExistingHealthConditions',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'Aggressive',
                        type: 'radio',
                        name: `Has your pet ever shown signs of aggression?`,
                        options: {
                            optionsKey: 'yes-no',
                            smallText: {
                                text: 'This includes attacking or biting a person or another animal, or showing signs of aggressive tendencies.'
                            }
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please confirm whether your pet has ever shown signs of aggression`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'PreExistingHealthConditions',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'PreExistingHealthConditions',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'Complaints',
                        type: 'radio',
                        name: `Has your pet been the subject of any complaints or legal action during the last 5 years?`,
                        options: {
                            optionsKey: 'yes-no'
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please confirm whether your pet has been subject to any complaints or legal action`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'PreExistingHealthConditions',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'PreExistingHealthConditions',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    }
                ]
            }
        ]
    },
    {
        key: 'your-details',
        type: 'step',
        name: 'Your Details',
        items: [
            {
                key: 'about-you',
                type: 'area',
                name: `About you`,
                intro: {
                    header: `Tell us about your pet's best friend`
                },
                items: [
                    {
                        key: 'OwnerTitle',
                        type: 'list',
                        name: `What's your title?`,
                        options: {
                            caption: 'Please select...',
                            optionsKey: 'owner-title'
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please select your title`,
                            }
                        ],
                    },
                    {
                        key: 'FirstName',
                        type: 'text',
                        name: `What's your first name?`,
                        options: {
                            placeholder: 'e.g. Joe',
                            helpText: `Why do we ask?
                            Insurers need your full name for your insurance certificate.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know your first name`,
                            },
                            {
                                type: 'maxLength',
                                max: 50,
                                message: `Oops - are you sure? This doesn't look right`
                            }
                        ]
                    },
                    {
                        key: 'LastName',
                        type: 'text',
                        name: `What's your last name?`,
                        options: {
                            placeholder: 'e.g. Bloggs',
                            helpText: `Why do we ask?
                            Insurers need your full name for your insurance certificate.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know your last name`,
                            },
                            {
                                type: 'maxLength',
                                max: 9,
                                message: `Oops - are you sure? This doesn't look right`
                            }
                        ]
                    },
                ]
            }
        ]
    },
    {
        key: 'their-insurance-options',
        type: 'step',
        name: 'Their insurance options',
        items: []
    }
]


export default layout;