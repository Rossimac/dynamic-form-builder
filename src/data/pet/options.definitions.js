const optionsDefinitions = [
    {
        key: 'yes-no',
        options: [
            {
                label: 'Yes',
                value: true,
            }, {
                label: 'No',
                value: false,
            }
        ],
    },
    {
        key: 'petType',
        options: [
            {
                label: 'Woof',
                value: 'D'
            }, {
                label: 'Meow',
                value: 'C'
            }
        ]
    },
    {
        key: 'gender',
        options: [
            {
                label: 'Boy',
                value: 'B'
            }, {
                label: 'Girl',
                value: 'G'
            }
        ]
    },
    {
        key: 'dog-breed-type',
        options: [
            {
                label: 'Pedigree',
                value: 'P',
            }, {
                label: 'Crossbreed',
                value: 'C',
            }, {
                label: 'Mongrel',
                value: 'M',
            },
        ]
    },
    {
        key: 'cat-breed-type',
        options: [
            {
                label: 'Pedigree',
                value: 'P',
            }, {
                label: 'Mixed Breed / Moggie',
                value: 'MBM',
            }
        ]
    },
    {
        key: 'pet-size',
        options: [
            {
                label: 'Small and sweet (up to 10kg)',
                value: 'S',
            }, {
                label: 'A happy medium (10 - 20kg)',
                value: 'M',
            }, {
                label: 'Large and in charge (more than 20kg)',
                value: 'L',
            }
        ]
    },
    {
        key: 'owner-title',
        options: [
            {
                label: 'Mr',
                value: 'MR',
            }, {
                label: 'Miss',
                value: 'MISS',
            }, {
                label: 'Mrs',
                value: 'MRS',
            }, {
                label: 'Ms',
                value: 'MS',
            }
        ]
    },
];

export default optionsDefinitions;
