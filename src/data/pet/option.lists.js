const optionsLists = [
    {
        key: 'yes-no',
        sourceList: 'yes-no',
        options: [
            true,
            false
        ],
    },
    {
        key: 'petType',
        sourceList: 'petType',
        options: [
            'D',
            'C'
        ]
    },
    {
        key: 'gender',
        sourceList: 'gender',
        options: [
            'B',
            'G'
        ]
    },
    {
        key: 'dog-breed-type',
        sourceList: 'dog-breed-type',
        options: [
            'P',
            'C',
            'M'
        ]
    },
    {
        key: 'cat-breed-type',
        sourceList: 'cat-breed-type',
        options: [
            'P',
            'MBM'
        ]
    },
    {
        key: 'pet-size',
        sourceList: 'pet-size',
        options: [
            'S',
            'M',
            'L'
        ]
    },
    {
        key: 'owner-title',
        sourceList: 'owner-title',
        options: [
            'MR',
            'MISS',
            'MRS',
            'MS'
        ]
    },
];

export default optionsLists;
