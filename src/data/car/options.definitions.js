const optionsDefinitions = [
    {
        key: 'yes-no',
        options: [
            {
                label: 'Yes',
                value: true,
            }, {
                label: 'No',
                value: false,
            }
        ],
    },
    {
        key: 'driver-side',
        options: [
            {
                label: 'Left',
                value: 'L',
            }, {
                label: 'Right',
                value: 'R',
            }
        ],
    }, {
        key: 'titles',
        options: [
            {
                label: 'Mr',
                value: '001',
            }, {
                label: 'Mrs',
                value: '002',
            }, {
                label: 'Ms',
                value: '003',
            }, {
                label: 'Miss',
                value: '004',
            }, {
                label: 'Dr',
                value: '005',
            }, {
                label: 'Sir',
                value: '006',
            },
        ],
    }, {
        key: 'owners-keepers',
        options: [
            {
                label: 'Employee',
                value: 'E',
            }, {
                label: 'Employer',
                value: 'F',
            }, {
                label: 'Garage',
                value: 'G',
            }, {
                label: 'Other',
                value: 'O',
            }, {
                label: 'Other family member',
                value: 'P',
            }, {
                label: 'You',
                value: 'Y',
            }, {
                label: 'Son or daughter',
                value: 'S',
            }, {
                label: 'Spouse',
                value: 'T',
            },
        ],
    }, {
        key: 'locations-day',
        options: [
            {
                label: 'On a driveway',
                value: 'D',
            }, {
                label: 'Office/Factory car park',
                value: 'OF',
            }, {
                label: 'On the street (away from home)',
                value: 'SA',
            },
            {
                label: 'Carport',
                value: 'C',
            }, {
                label: 'Locked Garage',
                value: 'L',
            }, {
                label: 'On the street (near home)',
                value: 'SN',
            }, {
                label: 'Locked compound',
                value: 'LC',
            }, {
                label: 'Open public car park',
                value: 'OP',
            }, {
                label: 'Secure public car park',
                value: 'SP',
            }, {
                label: 'Unlocked Compound',
                value: 'UC',
            }, {
                label: 'Unlocked Garage',
                value: 'UG',
            },
        ],
    }, {
        key: 'locations-night',
        options: [
            {
                label: 'Locked garage',
                value: 'L',
            }, {
                label: 'On a driveway',
                value: 'D',
            }, {
                label: 'On the street (near home)',
                value: 'SN',
            }, {
                label: 'On the street (away from home)',
                value: 'SA',
            },
            {
                label: 'Carport',
                value: 'C',
            }, {
                label: 'Third party premesis',
                value: 'T',
            }, {
                label: 'Locked compound',
                value: 'LC',
            }, {
                label: 'Office/Factory car park',
                value: 'OF',
            }, {
                label: 'Open public car park',
                value: 'OP',
            }, {
                label: 'Secure public car park',
                value: 'SP',
            }, {
                label: 'Unlocked Compound',
                value: 'UC',
            }, {
                label: 'Unlocked Garage',
                value: 'UG',
            },
        ],
    },
    {
        key: 'vehicle-ownership',
        options: [
            {
                label: 'Own another car',
                value: 'O',
            }, {
                label: 'Named driver on another policy',
                value: 'N',
            }, {
                label: 'Company car (including social use)',
                value: 'CSI',
            }, {
                label: 'Company car (excluding social use)',
                value: 'CSE',
            },
        ]
    },
    {
        key: 'vehicle-usage',
        options: [
            {
                label: 'Social Only',
                value: 'S',
            }, {
                label: 'Social and commuting',
                value: 'C',
            }, {
                label: 'Social, commuting and for business',
                value: 'B',
            },
        ]
    },
    {
        key: 'vehicle-business-usage',
        options: [
            {
                label: 'Business use by the policyholder',
                value: 'P',
            }, {
                label: 'Business use by the policyholder\s spouse',
                value: 'S',
            }, {
                label: 'Business use by the policyholder and their spouse',
                value: 'J',
            }, {
                label: 'Business use by any driver',
                value: 'A',
            },
        ]
    },
    {
        key: 'alarm-types',
        options: [
            {
                label: 'No Security Device',
                value: 'N',
            }, {
                label: 'Thatcham Approved Cat 1',
                value: 'T1'
            }, {
                label: 'Thatcham Approved Cat 2',
                value: 'T2'
            }, {
                label: 'Has a factory fitted alarm and immobiliser',
                value: 'FFAI',
            }, {
                label: 'Has a factory fitted immobiliser',
                value: 'FFI',
            }, {
                label: 'Has a non-factory fitted alarm and immobiliser',
                value: 'NFAI',
            }, {
                label: 'Has a non-factory fitted immobiliser',
                value: 'NFI',
            }
        ]
    },
    {
        key: 'import-types',
        options: [
            {
                label: 'Yes (Euro import - UK spec)',
                value: 'EUUK'
            }, {
                label: 'Yes (Euro import - not UK spec)',
                value: 'EU'
            }, {
                label: 'Yes (Japanese import)',
                value: 'J'
            }, {
                label: 'Yes (US import)',
                value: 'US'
            }
        ]
    },
    {
        key: 'gender',
        options: [
            {
                label: 'Male',
                value: 'M'
            }, {
                label: 'Female',
                value: 'F'
            }
        ]
    },
    {
        key: 'claim-types',
        options: [
            {
                label: 'Accident',
                value: 'A',
            }, {
                label: 'Theft',
                value: 'T'
            }, {
                label: 'Windscreen/Glass',
                value: 'WG'
            }, {
                label: 'Fire damage. At fault',
                value: 'FDAF'
            }, {
                label: 'Storm/Flood damage',
                value: 'SFD'
            }, {
                label: 'Vandalism damage',
                value: 'VD'
            }
        ]
    },
    {
        key: 'offence-types',
        options: [
            {
                label: 'Speeding',
                value: 'S',
            }, {
                label: 'Alcohol or drug related',
                value: 'AODR'
            }, {
                label: 'Careless driving',
                value: 'CD'
            }, {
                label: 'Inciting',
                value: 'I'
            }, {
                label: 'Aiding and abetting',
                value: 'AAA'
            }, {
                label: 'Failure to supply',
                value: 'FTS'
            }
        ]
    },
];

export default optionsDefinitions;
