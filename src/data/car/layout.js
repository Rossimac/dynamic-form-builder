const layout = [
    {
        key: 'VehicleMake',
        type: 'hidden'
    },
    {
        key: 'VehicleModel',
        type: 'hidden'
    },
    {
        key: 'VehicleManufactureDate',
        type: 'hidden'
    },

    {
        key: 's1',
        type: 'step',
        name: 'Your Car',
        items: [
            {
                key: 'a1',
                type: 'area',
                name: 'Your car',
                intro: {
                    header: 'Something else to tick off your to-do list...',
                    subHeader: 'Please answer all questions accurately and in full.',
                    hint: `Your policy will be sold to you on the basis of the information you provide, and if your answers are incorrect
                or incomplete, your policy could be cancelled, or you might have a claim rejected or not paid in full.`
                },
                items: [
                    // {
                    //     key: 'InceptionDate',
                    //     type: 'date-single-format',
                    //     name: 'When should the policy start?',
                    //     options: {
                    //         maximumDaysRelative: 30,
                    //         format: 'cccc ddd LLLL',
                    //         appendTodayAndTomorrow: true
                    //     },
                    //     validation: [
                    //         {
                    //             type: 'required',
                    //             required: true,
                    //             message: `Please let us know when you were born`,
                    //         },
                    //         {
                    //             days: 5,
                    //             type: 'dateGreaterThan',
                    //             message: `Oops - are you sure? This doesn't look right`,
                    //         },
                    //     ],
                    // },
                    {
                        key: 'AbiCode',
                        name: 'The car',
                        type: 'vehicle-summary',
                        options: {
                            resetQuestions: [
                                "AbiCode",
                                "VehicleMake",
                                "VehicleModel",
                                "DriverPosition",
                                "NumberOfSeats",
                                "Imported",
                                "AlarmType",
                                "Registration",
                                "VehicleManufactureDate",
                                "PurchasedDate",
                                "RegisteredOwnerAndKeeper",
                                "RegisteredOwner",
                                "RegisteredKeeper"
                            ],
                        },
                        visibility: {
                            all: [{
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: ''
                            }]
                        }
                    },
                    {
                        key: 'Registration',
                        type: 'vehicle-find',
                        name: 'What is the registration?',
                        options: {
                            uppercase: true,
                            mappedQuestions: [
                                { from: "DataItems.VehicleRegistration.EngineNumber", to: "AbiCode" },
                                { from: "DataItems.ClassificationDetails.Dvla.Make", to: "VehicleMake" },
                                { from: "DataItems.ClassificationDetails.Dvla.Model", to: "VehicleModel" },
                                { from: "DataItems.TechnicalDetails.General.DriverPosition", to: "DriverPosition" },
                                { from: "DataItems.TechnicalDetails.Dimensions.NumberOfSeats", to: "NumberOfSeats" },
                                { from: "DataItems.VehicleRegistration.Scrapped", to: "TrackingDeviceFitted" },
                                { from: "DataItems.VehicleRegistration.Imported", to: "Imported" },
                                { from: "DataItems.VehicleRegistration.Vrm", to: "Registration" },
                                { from: "DataItems.VehicleRegistration.DateFirstRegisteredUk", to: "PurchasedDate" },
                                { from: "DataItems.VehicleRegistration.DateFirstRegisteredUk", to: "VehicleManufactureDate" }
                            ],
                            resetQuestionsOnAnswer: [
                                "Purchased", "PurchasedDate", "RegisteredOwnerAndKeeper", "RegisteredOwner", "RegisteredKeeper"
                            ],
                            placeholder: 'e.g. ABC 1234',
                            helpText: `Brand new car?
                    Sometimes our system doesn’t recognise the registration numbers for brand new cars straight away.
                    
                    If your car is brand new, and has been released this year, please give us the details of the car manually. Don’t worry, this won’t take long.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please enter a registration number and click 'Find Car'`,
                            },
                            {
                                type: 'maxLength',
                                max: 10,
                                message: `Oops - are you sure? This doesn't look right`
                            }
                        ],
                        visibility: {
                            any: [{
                                fact: 'AbiCode',
                                operator: 'equal',
                                value: undefined
                            }, {
                                fact: 'AbiCode',
                                operator: 'equal',
                                value: ''
                            }]
                        }
                    },
                    {
                        key: 'AssumptionsList',
                        type: 'assumption-list',
                        name: 'We think this car:',
                        options: {
                            assumptions: [
                                'DriverPosition',
                                'NumberOfSeats',
                                'TrackingDeviceFitted',
                                'AlarmType',
                                'Imported'
                            ],
                        },
                        visibility: {
                            all: [{
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: ''
                            }, {
                                fact: 'DetailsCorrect',
                                operator: 'notEqual',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'DetailsCorrect',
                        type: 'radio',
                        name: 'Are these details correct?',
                        options: {
                            clearWhenInvisible: false,
                            optionsKey: 'yes-no',
                            helpText: `Do you need to change an answer?
                        Just select 'No' and then you can make a change to any of the associated questions.`
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know if the details above are correct`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: ''
                            }, {
                                fact: 'DetailsCorrect',
                                operator: 'notEqual',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'DriverPosition',
                        type: 'radio',
                        name: 'Is the car right or left hand drive?',
                        options: {
                            optionsKey: 'driver-side',
                            assumptionTexts: [
                                {
                                    text: "Is left hand drive",
                                    visibility: {
                                        all: [{
                                            fact: 'DriverPosition',
                                            operator: 'equal',
                                            value: 'L'
                                        }]
                                    }
                                }, {
                                    text: "Is right hand drive",
                                    visibility: {
                                        all: [{
                                            fact: 'DriverPosition',
                                            operator: 'equal',
                                            value: 'R'
                                        }]
                                    }
                                }
                            ]
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know if the car is right or left hand drive`,
                            }
                        ]
                        ,
                        visibility: {
                            all: [{
                                fact: 'DetailsCorrect',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'NumberOfSeats',
                        type: 'number-select',
                        name: 'How many seats does the car have?',
                        options: {
                            lowerLimit: 1,
                            upperLimit: 9,
                            assumptionTexts: [
                                {
                                    template: "Has {0} seats",
                                    mergeVariables: [
                                        'NumberOfSeats'
                                    ]
                                }
                            ],
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know the number of car seats`,
                            },
                            {
                                type: 'min',
                                min: 1,
                                message: `Oops - are you sure? This doesn't look right`
                            },
                            {
                                type: 'max',
                                max: 9,
                                message: `Oops - are you sure? This doesn't look right`
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'DetailsCorrect',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'TrackingDeviceFitted',
                        type: 'radio',
                        name: 'Does it have a tracking device?',
                        options: {
                            optionsKey: 'yes-no',
                            assumptionTexts: [
                                {
                                    text: "Has a tracker device fitted",
                                    visibility: {
                                        all: [{
                                            fact: 'TrackingDeviceFitted',
                                            operator: 'equal',
                                            value: true
                                        }]
                                    }
                                },
                                {
                                    text: "Has no tracker device fitted",
                                    visibility: {
                                        any: [{
                                            fact: 'TrackingDeviceFitted',
                                            operator: 'equal',
                                            value: undefined
                                        }, {
                                            fact: 'TrackingDeviceFitted',
                                            operator: 'equal',
                                            value: false
                                        }]
                                    }
                                }
                            ]
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know if a tracking device is fitted`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'DetailsCorrect',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'AlarmType',
                        type: 'radio',
                        name: 'What type of alarm does the car have?',
                        options: {
                            caption: 'Select alarm type',
                            optionsKey: 'alarm-types',
                            otherOptionsKey: 'alarm-types-other',
                            assumptionTexts: [
                                {
                                    text: "Has an alarm",
                                    visibility: {
                                        all: [{
                                            fact: 'AlarmType',
                                            operator: 'notEqual',
                                            value: undefined
                                        }, {
                                            fact: 'AlarmType',
                                            operator: 'notEqual',
                                            value: 'N'
                                        }]
                                    }
                                },
                                {
                                    text: "Has no alarm",
                                    visibility: {
                                        any: [{
                                            fact: 'AlarmType',
                                            operator: 'equal',
                                            value: undefined
                                        }, {
                                            fact: 'AlarmType',
                                            operator: 'equal',
                                            value: 'N'
                                        }]
                                    }
                                }
                            ]
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know the of any security devices fitted`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'DetailsCorrect',
                                operator: 'equal',
                                value: false
                            }]
                        }
                    },
                    {
                        key: 'Imported',
                        type: 'radio',
                        name: 'Is the car an import?',
                        options: {
                            optionsKey: 'yes-no',
                            helpText: `Why does this matter?
                        Insurers need to know the exact car you have so they can estimate the cost of repairs.`,
                            assumptionTexts: [
                                {
                                    text: "Is imported",
                                    visibility: {
                                        all: [{
                                            fact: 'Imported',
                                            operator: 'equal',
                                            value: true
                                        }]
                                    }
                                },
                                {
                                    text: "Is not imported",
                                    visibility: {
                                        all: [{
                                            fact: 'Imported',
                                            operator: 'equal',
                                            value: false
                                        }]
                                    }
                                }
                            ]
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know if the car is an import`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'DetailsCorrect',
                                operator: 'equal',
                                value: false
                            }]
                        },
                        items: [{
                            key: 'ImportedFrom',
                            type: 'radio',
                            name: 'Where was the car imported from?',
                            options: {
                                optionsKey: 'import-types'
                            },
                            validation: [
                                {
                                    type: 'required',
                                    onlyIf: {
                                        all: [
                                            {
                                                fact: 'Imported',
                                                operator: 'equal',
                                                value: true
                                            },
                                        ]
                                    },
                                    message: `Please let us know who the owner is`,
                                }
                            ],
                            visibility: {
                                all: [{
                                    fact: 'Imported',
                                    operator: 'notEqual',
                                    value: undefined
                                }, {
                                    fact: 'Imported',
                                    operator: 'equal',
                                    value: true
                                }]
                            }
                        },],
                    },
                    {
                        key: 'Modified',
                        type: 'radio',
                        name: 'Has the car been modified in any way?',
                        options: {
                            optionsKey: 'yes-no',
                            helpText: `What does this mean?
                        If you or a previous owner has made a change from the manufacturer's original specification, such as alloy wheels, air conditioning, bodywork, exhaust system, suspension or tinted windows, add it here.
                        
                        If you’re unsure if your car’s been modified, check its previous history to find out.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know of any modifications to the car`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: ''
                            }]
                        },
                        collection: {
                            options: {
                                max: 5,
                                autoSave: true,
                                individualRemovalUndo: false,
                                groupRemovalUndo: true,
                                groupRemovalWarning: false
                            },
                            summaryTitle: {
                                text: "This is the title"
                            },
                            summarySubtitle: {
                                text: "More information"
                            },
                            items: [
                                {
                                    key: 'Modification',
                                    type: 'composite',
                                    name: 'How has the car been modified?',
                                    options: {
                                        smallText: {
                                            text: `You can add as many modifications as you like.`,
                                        },
                                    },
                                    validation: [
                                        {
                                            type: 'required',
                                            required: true,
                                            message: `Let us know about the car's modifications`,
                                        }
                                    ],
                                    items: [
                                        {
                                            key: 'ModificationCategory',
                                            type: 'list',
                                            options: {
                                                caption: 'Select category',
                                                optionsKey: 'modification-categories'
                                            }
                                        },
                                        {
                                            key: 'ModificationType',
                                            type: 'list',
                                            options: {
                                                caption: 'Select type',
                                                optionsKey: 'modification-types'
                                            }
                                        },
                                    ]
                                },
                            ]
                        }
                    },
                    {
                        key: 'Valuation',
                        type: 'currency',
                        name: 'Roughly how much is the car worth?',
                        options: {
                            min: 0,
                            optionsKey: 'yes-no',
                            helpText: `How come there's a value already there?
                        To make things easier for you, we use a car industry valuation service to show you how much the car is worth.
                        
                        Not happy with the value?
                        The insurer will normally pay the trade value we have shown you. However, if you think the value is wrong, you can change it.
                        
                        What if there is no value in here?
                        Tell us how much the car would be worth if you sold it today. Or tell us how much you’re buying the car for.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                onlyIf: {
                                    all: [
                                        {
                                            fact: 'AbiCode',
                                            operator: 'notEqual',
                                            value: ''
                                        }
                                    ],
                                },
                                message: `Please let us know how much the car is worth`,
                            },
                            {
                                type: 'min',
                                min: 1,
                                message: `Oops - are you sure? This doesn't look right`
                            },
                            {
                                type: 'max',
                                max: 999999,
                                message: `Oops - are you sure? This doesn't look right`
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'AbiCode',
                                operator: 'notEqual',
                                value: ''
                            }]
                        }
                    },
                    {
                        key: 'Purchased',
                        type: 'radio',
                        name: 'Do you have the car yet?',
                        options: {
                            optionsKey: 'yes-no',
                            smallText: {
                                text: 'If you do, we’ll automatically email you MOT, tax and insurance renewal reminders.'
                            },
                            helpText: `What if you don't have the car yet?
                    If you’re looking for insurance for a car you haven’t got yet, that’s no problem – you can search by the make and model of car you’d like.
                    
                    MOT, tax and insurance reminders
                    As part of Car Monitor, we’ll automatically email you MOT, tax and insurance reminders… so you’ll never miss your renewal dates.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know if you have the car yet`,
                            }
                        ],
                        items: [
                            {
                                key: 'PurchasedDate',
                                type: 'date',
                                name: 'When did you buy the car?',
                                options: {
                                    confirmFirstVisit: false,
                                    min: {
                                        type: 'question',
                                        questionId: 'VehicleManufactureDate'
                                    },
                                    day: {
                                        visible: false,
                                        caption: 'Day',
                                        defaultValue: 1
                                    },
                                    month: {
                                        visible: true,
                                        caption: 'Month'
                                    },
                                    year: {
                                        visible: true,
                                        caption: 'Year'
                                    },
                                    helpText: `Why do we ask?
                                    Insurers need to know who owns the car, and whether it’s owned and kept by different people.`,
                                },
                                validation: [
                                    {
                                        type: 'required',
                                        required: true,
                                        message: `Please let us know when you bought the car`,
                                    }
                                ],
                                visibility: {
                                    all: [{
                                        fact: 'AbiCode',
                                        operator: 'notEqual',
                                        value: undefined
                                    }, {
                                        fact: 'AbiCode',
                                        operator: 'notEqual',
                                        value: ''
                                    }, {
                                        fact: 'Purchased',
                                        operator: 'notEqual',
                                        value: undefined
                                    }, {
                                        fact: 'Purchased',
                                        operator: 'equal',
                                        value: true
                                    }]
                                },
                            },
                        ],
                    },
                    {
                        key: 'RegisteredOwnerAndKeeper',
                        type: 'radio',
                        name: 'Are you the owner and registered keeper of the car (or will you be)?',
                        options: {
                            optionsKey: 'yes-no',
                            smallText: {
                                text: 'If your name is on the V5C \'log book\' then you are the registered keeper'
                            },
                            helpText: `What's the difference between owner and registered keeper?
                    If your name is on the V5C vehicle registration certificate (often called the logbook), you’re the registered keeper.
                    
                    If you bought the car, or received it as a gift, you will own it too.
                    
                    Sometimes a company (such as a leasing company) or another person might own a car and be the registered keeper of the car, so please make sure you check your V5C.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know if you're the owner and registered keeper`,
                            }
                        ],
                        items: [{
                            key: 'RegisteredOwner',
                            type: 'list',
                            name: 'Who is the owner of the car?',
                            options: {
                                caption: 'Select owner',
                                optionsKey: 'owners-keepers',
                                helpText: `Why do we ask?
                        Insurers need to know who owns the car, and whether it’s owned and kept by different people.`,
                            },
                            validation: [
                                {
                                    type: 'required',
                                    onlyIf: {
                                        all: [
                                            {
                                                fact: 'RegisteredOwnerAndKeeper',
                                                operator: 'equal',
                                                value: false
                                            }
                                        ],
                                    },
                                    message: `Please let us know who the owner is`,
                                }
                            ],
                            visibility: {
                                all: [{
                                    fact: 'RegisteredOwnerAndKeeper',
                                    operator: 'notEqual',
                                    value: undefined
                                }, {
                                    fact: 'RegisteredOwnerAndKeeper',
                                    operator: 'equal',
                                    value: false
                                }]
                            }
                        }, {
                            key: 'RegisteredKeeper',
                            type: 'list',
                            name: 'Who is the registered keeper of the car?',
                            options: {
                                caption: 'Select registered keeper',
                                optionsKey: 'owners-keepers',
                                helpText: `Wondering how to find out?
                        Please check your V5C vehicle registration certificate (often called the logbook) to find out who the registered keeper is.
                        
                        If you can’t find the V5C, you can get the vehicle information from DVLA online.`,
                            },
                            validation: [
                                {
                                    type: 'required',
                                    onlyIf: {
                                        all: [
                                            {
                                                fact: 'RegisteredOwnerAndKeeper',
                                                operator: 'equal',
                                                value: false
                                            }
                                        ],
                                    },
                                    message: `Please let us know who the registered keeper is`,
                                }
                            ],
                            visibility: {
                                all: [{
                                    fact: 'RegisteredOwnerAndKeeper',
                                    operator: 'notEqual',
                                    value: undefined
                                }, {
                                    fact: 'RegisteredOwnerAndKeeper',
                                    operator: 'equal',
                                    value: false
                                }]
                            }
                        }],
                    }],
            }, {
                key: 'a2',
                type: 'area',
                name: 'Using your car',
                items: [{
                    key: 'Usage',
                    type: 'radio',
                    name: 'How do you use the car?',
                    options: {
                        optionsKey: 'vehicle-usage',
                        helpText: `Why does this matter?
                    Insurers look at how you and the named drivers on the policy use your car to work out how much risk there is in insuring it.
                    
                    See which option most closely matches your needs:
                    
                    Social only – Choose this if you and the named drivers on the policy only use the car for personal driving and never drive to work or to where you study, and never drive for work either.
                    
                    Social and commuting – Choose this if you or a named driver drives to and from a single place of work or where you study, but do no other business travel.
                    
                    Social, commuting and for business – choose this if you or anyone named on the policy uses the car on business, away from a single place of work. Please note, this doesn’t cover you for commercial travelling, i.e. if you use your car for sales or delivering goods and/or services.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know how you use your car`,
                        }
                    ],
                    items: [{
                        key: 'BusinessUsage',
                        type: 'radio',
                        name: 'Who uses the car for business?',
                        options: {
                            optionsKey: 'vehicle-business-usage',
                            helpText: `Wondering what’s the right option?
                        We'll ask you about the other people you’d like to add to your policy soon. If your partner uses the car for business but you’re not married, choose the final option.
                        
                        Only me, the policyholder – choose this if you use the car on business away from your single place of work.
                        
                        Only my spouse – choose this if your husband or wife uses the car on business away from their single place of work.
                        
                        Both me and my spouse – choose this if you both use the car on business away from your individual places of work.
                        
                        Any driver named on the insurance – choose this if anyone else you’re insuring to drive the car uses it for business.`,
                            notification: {
                                titleText: 'Please note',
                                bodyText: 'For this selection you will need to add an additional driver when you reach the \'About you\' section.',
                                visibility: {
                                    all: [{
                                        fact: 'BusinessUsage',
                                        operator: 'notEqual',
                                        value: undefined
                                    }, {
                                        fact: 'BusinessUsage',
                                        operator: 'notEqual',
                                        value: 'P'
                                    }]
                                }
                            }
                        },
                        validation: [
                            {
                                type: 'required',
                                onlyIf: {
                                    all: [
                                        {
                                            fact: 'Usage',
                                            operator: 'equal',
                                            value: 'B'
                                        }
                                    ],
                                },
                                message: `Please let us know who uses the car for business`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'Usage',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'Usage',
                                operator: 'equal',
                                value: 'B'
                            }]
                        }
                    }, {
                        key: 'BusinessMileage',
                        type: 'number',
                        name: 'How many business miles does the car do a year?',
                        options: {
                            min: 0,
                            placeholder: 'e.g. 5000',
                            helpText: `Wondering how to work this out?
                        We’re after the number of business miles you think your car will do over the next year.
                        
                        Working out number of miles your car’s done this year is probably a good starting point. Think about how many business miles you usually drive per day or per week, then multiply it up to get a yearly figure. Don’t forget to take off any holiday weeks, when you’re not doing business miles.
                        
                        Insurers use 500 business miles per year as a minimum. If you think you’ll do fewer than that, round it up to 500 business miles.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                onlyIf: {
                                    all: [
                                        {
                                            fact: 'Usage',
                                            operator: 'equal',
                                            value: 'B'
                                        }
                                    ],
                                },
                                message: `Please enter how many business miles the car does in a year`,
                            },
                            {
                                type: 'min',
                                min: 500,
                                message: `Insurers use 500 miles per year as a minimum. If you think you'll do fewer than that, round it up to 500 miles`
                            },
                            {
                                type: 'max',
                                max: 999999,
                                message: `Oops - are you sure? This doesn't look right`
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'Usage',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'Usage',
                                operator: 'equal',
                                value: 'B'
                            }]
                        }
                    }],
                },
                {
                    key: 'PersonalMileage',
                    type: 'number',
                    name: 'How many personal miles does the car do a year?',
                    options: {
                        min: 0,
                        placeholder: 'e.g. 5000',
                        smallText: {
                            text: 'Please include social driving and commuting',
                            visibility: {
                                all: [{
                                    fact: 'Usage',
                                    operator: 'notEqual',
                                    value: undefined
                                }, {
                                    fact: 'Usage',
                                    operator: 'equal',
                                    value: 'B'
                                }]
                            }
                        },
                        helpText: `Brand new car?
                    Sometimes our system doesn’t recognise the registration numbers for brand new cars straight away.
                    
                    If your car is brand new, and has been released this year, please give us the details of the car manually. Don’t worry, this won’t take long.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please enter how many personal miles the car does in a year`,
                        },
                        {
                            type: 'min',
                            min: 500,
                            message: `Insurers use 500 miles per year as a minimum. If you think you'll do fewer than that, round it up to 500 miles`
                        },
                        {
                            type: 'max',
                            max: 999999,
                            message: `Oops - are you sure? This doesn't look right`
                        }
                    ]
                },
                {
                    key: 'DayLocation',
                    type: 'radio',
                    name: 'Where is the car kept during the day?',
                    options: {
                        caption: 'Select location',
                        optionsKey: 'locations-day',
                        otherOptionsKey: 'locations-day-other',
                        helpText: `Why are we asking?
                    Insurers look at where your car is kept during the day to work out how safe it is likely to be while it isn't being driven.
                    
                    What if where I keep it varies?
                    Insurers understand that your car is likely to be kept in different places depending on what you're doing. So pick the answer that is true for most of the time - the one that best reflects where your car is kept during the day.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know where you keep the car in the day`,
                        }
                    ],
                },
                {
                    key: 'NightLocation',
                    type: 'radio',
                    name: 'Where is the car kept at night?',
                    options: {
                        caption: 'Select location',
                        optionsKey: 'locations-night',
                        otherOptionsKey: 'locations-night-other',
                        helpText: `Why are we asking?
                    Cars are more likely to be broken into at night, so insurers look at how safe your car is overnight to help work out the price of your policy.
                    
                    What if where I keep it varies?
                    Insurers understand that your car might be kept in different places depending on what you're doing. Pick the answer that is true for most of the time - where you most often leave your car at night.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know where you keep the car at night`,
                        }
                    ],
                }, {
                    key: 'OtherCars',
                    type: 'radio',
                    name: 'Do you drive any other cars?',
                    options: {
                        optionsKey: 'yes-no',
                        helpText: `Why does it matter?
                    If you drive more than one car, you might be charged less for this policy because you're spending less time driving this car`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know if you drive any other cars`,
                        }
                    ],
                    items: [{
                        key: 'OtherCarsType',
                        type: 'radio',
                        name: 'Which of these do you drive?',
                        options: {
                            optionsKey: 'vehicle-ownership',
                            smallText: {
                                text: 'If you drive several other cars, pick the one you drive most.'
                            },
                            helpText: `Wondering what to choose?
                        Let us know which of these you drive.
                        
                        Own another car – choose this if you have another car of your own.
                        
                        Named driver on another policy – choose this if you’re insured to drive someone else’s car.
                        
                        Company car, for business and social use – choose this if you use your company car for social driving too.
                        
                        Company car, for business use only – choose this if you use your company car only for business.
                        
                        Insurers don’t need to know if you occasionally drive hire cars or belong to a car club, so don’t worry about that.`,
                        },
                        validation: [
                            {
                                type: 'required',
                                onlyIf: {
                                    all: [
                                        {
                                            fact: 'OtherCars',
                                            operator: 'equal',
                                            value: true
                                        }
                                    ],
                                },
                                message: `Please let us know which other cars you drive`,
                            }
                        ],
                        visibility: {
                            all: [{
                                fact: 'OtherCars',
                                operator: 'notEqual',
                                value: undefined
                            }, {
                                fact: 'OtherCars',
                                operator: 'equal',
                                value: true
                            }]
                        }
                    },],
                }
                ],
            }],
    }, {
        key: 's2',
        type: 'step',
        name: 'Your Details',
        items: [{
            key: 's2-a1',
            type: 'area',
            name: 'Personal information',
            items: [
                {
                    key: 'FirstName',
                    type: 'text',
                    name: `What's your first name?`,
                    options: {
                        placeholder: 'e.g. Joe',
                        helpText: `Why do we ask?
                        Insurers need your full name for your insurance certificate.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know your first name`,
                        },
                        {
                            type: 'maxLength',
                            max: 50,
                            message: `Oops - are you sure? This doesn't look right`
                        }
                    ]
                },
                {
                    key: 'LastName',
                    type: 'text',
                    name: `What's your last name?`,
                    options: {
                        placeholder: 'e.g. Bloggs',
                        helpText: `Why do we ask?
                        Insurers need your full name for your insurance certificate.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know your last name`,
                        },
                        {
                            type: 'maxLength',
                            max: 9,
                            message: `Oops - are you sure? This doesn't look right`
                        }
                    ]
                },
                {
                    key: 'DateOfBirth',
                    type: 'date',
                    name: 'What is your date of birth?',
                    options: {
                        displayAsLists: true,
                        minYears: -110,
                        maxDays: 30,
                        day: {
                            visible: true,
                            caption: 'Day'
                        },
                        month: {
                            visible: true,
                            caption: 'Month'
                        },
                        year: {
                            visible: true,
                            caption: 'Year'
                        },
                        helpText: `Why do we ask?
                    Insurance premiums reflect how old you are. Insurers keep track of the number of claims made by different age groups and vary their prices accordingly.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know when you were born`,
                        },
                        {
                            type: 'dateGreaterThan',
                            years: -100,
                            message: `Oops - are you sure? This doesn't look right`,
                        },
                        {
                            type: 'dateLessThan',
                            message: `Oops - are you sure? This doesn't look right`,
                        },
                        {
                            type: 'dateNotBetween',
                            lowYears: -17,
                            lowDays: 30,
                            message: `Oops - it doesn't look like you're old enough to drive a car`,
                        }
                    ],
                },
                {
                    key: 'Gender',
                    type: 'radio',
                    name: 'Are you:',
                    options: {
                        optionsKey: 'gender',
                        helpText: `This information will not impact your individual insurance pricing. However, we appreciate that some people may not identify with a binary gender. In this case, please refer to your sex/gender as outlined on your birth certificate or gender recognition certificate.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know what gender you are`,
                        }
                    ]
                }
            ],
        },
        {
            key: 's2-a2',
            type: 'area',
            name: 'Claims & Convictions',
            items: [
                {
                    key: 'AnyClaims',
                    type: 'radio',
                    name: 'Have you had any motor accidents, claims or losses in the last five years?',
                    options: {
                        optionsKey: 'yes-no',
                        smallText: {
                            text: 'This is regardless of who/what was at fault or if a claim was made or not. If you don’t tell your insurer about previous accidents, claims or losses, your car insurance may not pay out if you make a claim'
                        },
                        helpText: `What does this include?

                        You must declare any claim that has been made on your policy involving any type of motor vehicle, such as cars, vans and motorbikes.

                        This includes claims made by a named driver on your policy, regardless as to whether this person remains on your policy or not.

                        If your claim is still being processed, you need to declare this too. If you're unsure at this time who was at fault, please declare that you, the policyholder were at fault.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know whether you've had any incidents or made any claims`,
                        }
                    ],
                    collection: {
                        options: {
                            summaryLabel: 'Your incidents/claims:',
                            addAnotherText: 'Add another incident/claim',
                            max: 5,
                            autoSave: false,
                            individualRemovalUndo: true,
                            groupRemovalUndo: false,
                            groupRemovalWarning: true,
                            groupRemovalWarningText: `Are you sure you want to remove this? If you don't tell your insurer about previous incidents or claims, your car insurance may not pay out`,
                            noSavedItemsErrorMessage: 'Please add any driving incidents or claims, or select No',
                            summaryTitle: {
                                template: "01/09/2019",
                                mergeVariables: [
                                    'NumberOfSeats'
                                ]
                            },
                            summarySubtitle: {
                                template: "Accident. At fault",
                                mergeVariables: [
                                    'NumberOfSeats'
                                ]
                            },
                            summaryDetail: {
                                template: "No claim discount is affected",
                                mergeVariables: [
                                    'NumberOfSeats'
                                ]
                            },
                        },
                        items: [
                            {
                                key: 'ClaimType',
                                type: 'radio',
                                name: 'What kind of incident or claim was it?',
                                options: {
                                    caption: 'Please select',
                                    optionsKey: 'claim-types',
                                    otherOptionsKey: 'claim-types-other',
                                },
                                validation: [
                                    {
                                        type: 'required',
                                        required: true,
                                        message: `Please pick the type of incident or claim`,
                                    }
                                ]
                            },
                            {
                                key: 'ClaimDate',
                                type: 'date',
                                name: 'When did the incident happen?',
                                options: {
                                    displayAsLists: true,
                                    minYears: -5,
                                    day: {
                                        visible: true,
                                    },
                                    month: {
                                        visible: true,
                                    },
                                    year: {
                                        visible: true,
                                    },
                                    helpText: `Can't quite remember?
                                    Take a moment to look up any paperwork you might have on the incident.`,
                                },
                                validation: [
                                    {
                                        type: 'required',
                                        required: true,
                                        message: `Please enter the date when the incident happened`,
                                    },
                                    {
                                        type: 'dateGreaterThan',
                                        years: -5,
                                        message: `We only need to know about claims or incidents from the last 5 years`,
                                    },
                                    {
                                        type: 'dateLessThan',
                                        message: `Oops - are you sure? This doesn't look right`,
                                    },
                                ],
                            },
                            {
                                key: 'NcbAffected',
                                type: 'radio',
                                name: 'Did this claim affect your no claims discount?',
                                options: {
                                    optionsKey: 'yes-no',
                                    helpText: `Not sure how to find out?
                                    Your current insurance documents should tell you how many years of no claims discount you have, so you should be able to work out whether your no claims discount was affected.`,
                                },
                                validation: [
                                    {
                                        type: 'required',
                                        required: true,
                                        message: `Please let us know if it affected your no claims discount`,
                                    }
                                ]
                            }
                        ]
                    }
                },
                {
                    key: 'AnyConvictions',
                    type: 'radio',
                    name: 'Have you committed any driving offences or had any Fixed Penalty Notices in the last 5 years?',
                    options: {
                        optionsKey: 'yes-no',
                        smallText: {
                            text: 'If you don’t tell your insurer about previous offences, your car insurance may not pay out if you make a claim'
                        },
                        helpText: `Wondering what counts?
                        Insurers need to know about any driving offences you've been convicted of, including any Fixed Penalty Notices (FPNs) you've been issued for offences such as speeding, driving without due care and attention or driving with blood/alcohol level above the limit.
                        
                        If you've attended a speed awareness course, you don't have to declare this as a conviction.
                        
                        Penalty Charge Notices (PCNs) issued for parking offences don't have to be declared either.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know if you have had any driving offences or fixed penalty notices in the last 5 years`,
                        }
                    ],
                    collection: {
                        options: {
                            summaryLabel: 'Your offences:',
                            addAnotherText: 'Add another offence',
                            summaryMutedDetailText: '{fullDate}',
                            max: 5,
                            autoSave: false,
                            individualRemovalUndo: true,
                            groupRemovalUndo: false,
                            groupRemovalWarning: true,
                            groupRemovalWarningText: `Are you sure you want to remove this? If you don't tell your insurer about previous offences, your car insurance may not pay out`,
                            noSavedItemsErrorMessage: 'Please add any driving offences or fixed penalty notices, or select No',
                            summaryTitle: {
                                template: "01/09/2019",
                                mergeVariables: [
                                    'NumberOfSeats'
                                ]
                            },
                            summarySubtitle: {
                                template: "SP30 - Exceeding Statutory Speed Limit On a Public Road",
                                mergeVariables: [
                                    'NumberOfSeats'
                                ]
                            },
                            summaryDetail: {
                                template: "No points, £70 fine, 8 month ban",
                                mergeVariables: [
                                    'NumberOfSeats'
                                ]
                            },
                        },
                        summaryTitle: {
                            template: "This is the title",
                            mergeVariables: [
                                'NumberOfSeats'
                            ]
                        },
                        summarySubtitle: {
                            template: "More information",
                            mergeVariables: [
                                'NumberOfSeats'
                            ]
                        },
                        items: [
                            {
                                key: 'OffenceType',
                                type: 'radio',
                                name: 'What kind of offence was it?',
                                options: {
                                    caption: 'Please select',
                                    optionsKey: 'offence-types',
                                    otherOptionsKey: 'offence-types-other',
                                },
                                validation: [
                                    {
                                        type: 'required',
                                        required: true,
                                        message: `Please pick the type of incident or claim`,
                                    }
                                ]
                            },
                            // {
                            //     key: 'IncidentDate',
                            //     type: 'date',
                            //     name: 'When did the incident happen?',
                            //     options: {
                            //         dayShown: true,
                            //         monthShown: true,
                            //         yearShown: true,
                            //         dayCaption: 'DD',
                            //         monthCaption: 'MM',
                            //         yearCaption: 'YY',
                            //         smallText: {
                            //             text: "You can find the right date here on your driving record"
                            //         },
                            //         helpText: `Can't quite remember?
                            //         Take a moment to look up any paperwork you might have on the incident.`,
                            //     },
                            //     validation: []
                            //{
                            //         type: 'required',
                            //         required: true,
                            //         message: `Please enter the date when the incident happened`,
                            //     
                            //]}
                            // },
                        ]
                    }
                },
                {
                    key: 'NonDrivingConvictions',
                    type: 'radio',
                    name: 'Do you have any unspent non-driving convictions?',
                    options: {
                        optionsKey: 'yes-no',
                        helpText: `Why do we ask?
                        If you have any unspent convictions, it’s really important to tell your insurance company. If you don’t tell them, your insurance may not be valid and any claim is likely to be rejected.`,
                    },
                    validation: [
                        {
                            type: 'required',
                            required: true,
                            message: `Please let us know if you have any unspent non-driving convictions`,
                        }
                    ]
                },
            ],

        }],
    }, {
        key: 'step-your-policy',
        type: 'step',
        name: 'Your Policy',
        items: [
            {
                key: 'area-your-policy',
                type: 'area',
                name: 'Your policy',
                items: [
                    {
                        key: 'InceptionDate',
                        type: 'date-single-format',
                        name: 'When would you like your insurance to start?',
                        options: {
                            smallText: {
                                text: 'Pick a date between today and up to 30 days from now. We’ll also use this to email you reminders of when it’s time to renew.'
                            },
                            helpText: `Wondering which day to pick?
                        Make sure there’s no gap, so you’re always insured for this vehicle. If you have an existing insurance policy, your new one should start immediately after it finishes.
                        
                        You can normally obtain and 'lock-in' prices up to 30 days ahead of your renewal. Prices obtained in advance will usually be lower than those obtained in the days immediately prior to your renewal date.`,
                            maximumDaysRelative: 30,
                            format: 'cccc ddd LLLL',
                            dateDivider: 'days',
                            appendTodayAndTomorrow: true
                        },
                        validation: [
                            {
                                type: 'required',
                                required: true,
                                message: `Please let us know when to start the policy`,
                            },
                            {
                                type: 'dateGreaterThan',
                                basedOn: 'DateOfBirth',
                                years: 17,
                                days: 2,
                                                                message: `Oops - It doesn’t look like you will be old enough to drive a car at this policy start date`,
                            },
                        ],
                    },
                ],

            }]
    },
    {
        key: 'their-insurance-options',
        type: 'step',
        name: 'Your Quotes',
        items: []
    }];



export default layout;