const optionsLists = [
    {
        key: 'yes-no',
        sourceList: 'yes-no',
        options: [
            true,
            false
        ],
    },
    {
        key: 'driver-side',
        sourceList: 'driver-side',
        options: [
            'L',
            'R'
        ],
    },
    {
        key: 'modification-categories',
        sourceList: 'modification-categories',
        options: [
            'SBK',
            'BM',
            'PW',
            'SS',
            'WT',
            'ET',
            'B',
            'A'
        ],
    },
    {
        key: 'modification-types-sbk',
        sourceList: 'modification-types',
        options: [
            'RSA',
            'RRS',
            'RV',
            'FSAD',
            'SSSS',
            'CBK'
        ],
    },
    {
        key: 'modification-types-bm',
        sourceList: 'modification-types',
        options: [
            'FW',
            'FWA',
            'BB',
            'FBP',
            'SB',
            'AW'
        ],
    },
    {
        key: 'owners-keepers',
        sourceList: 'owners-keepers',
        options: [
            'E',
            'F',
            'G',
            'O',
            'Y',
            'S',
            'T'
        ],
    },
    {
        key: 'locations-day',
        sourceList: 'locations-day',
        options: [
            'D',
            'OF',
            'SA'
        ],
    },
    {
        key: 'locations-day-other',
        sourceList: 'locations-day',
        options: [
            'C',
            'L',
            'SN',
            'LC',
            'OP',
            'SP',
            'UC',
            'UG'
        ],
    },
    {
        key: 'locations-night',
        sourceList: 'locations-night',
        options: [
            'L',
            'D',
            'SA',
            'SN'
        ]
    },
    {
        key: 'locations-night-other',
        sourceList: 'locations-night',
        options: [
            'C',
            'T',
            'LC',
            'OF',
            'OP',
            'SP',
            'UC',
            'UG'
        ]
    },
    {
        key: 'vehicle-ownership',
        sourceList: 'vehicle-ownership',
        options: [
            'O',
            'N',
            'CSI',
            'CSE'
        ]
    },
    {
        key: 'vehicle-usage',
        sourceList: 'vehicle-usage',
        options: [
            'S',
            'C',
            'B'
        ]
    },
    {
        key: 'vehicle-business-usage',
        sourceList: 'vehicle-business-usage',
        options: [
            'P',
            'S',
            'J',
            'A'
        ]
    },
    {
        key: 'alarm-types',
        sourceList: 'alarm-types',
        options: [
            'N',
            'T1',
            'T2'
        ]
    },
    {
        key: 'alarm-types-other',
        sourceList: 'alarm-types',
        options: [
            'FFAI',
            'FFI',
            'NFAI',
            'NFI'
        ]
    },
    {
        key: 'import-types',
        sourceList: 'import-types',
        options: [
            'EUUK',
            'EU',
            'J',
            'US'
        ]
    },
    {
        key: 'gender',
        sourceList: 'gender',
        options: [
            'M',
            'F'
        ]
    },
    {
        key: 'claim-types',
        sourceList: 'claim-types',
        options: [
            'A',
            'T',
            'WG'
        ]
    },
    {
        key: 'claim-types-other',
        sourceList: 'claim-types',
        options: [
            'FDAF',
            'SFD',
            'VD'
        ]
    },
    {
        key: 'offence-types',
        sourceList: 'offence-types',
        options: [
            'S',
            'AODR',
            'CD'
        ]
    },
    {
        key: 'offence-types-other',
        sourceList: 'offence-types',
        options: [
            'I',
            'AAA',
            'FTS'
        ]
    },
];

export default optionsLists;
