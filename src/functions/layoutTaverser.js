import ko from 'knockout';

let layoutCache = [];
let validationIds = [];

function traverseLayout(stateItems, { key, type, name, items, options, intro, validation, visibility, collection }, index, groupKey, groupId, selectedStep) {

    const item = {
        id: ko.observable(key),
        ordinal: ko.observable(index),
        key: key,
        type: type,
        name: name
    };

    if (items !== undefined) {
        item.items = ko.observableArray(items.map((value, itemIndex) => {
            return traverseLayout(stateItems, value, itemIndex, groupKey, groupId);
        }).flat());
    }

    if (type === 'step') {
        item.done = false;
        item.validatableIds = ko.observableArray(validationIds);
        //reset Ids for next step
        validationIds = [];
    }

    let cachedItem = layoutCache.find(element => element.id === item.id);

    if (cachedItem === undefined) {
        cachedItem = {
            id: item.id(),
            observable: ko.observable()
        };
        layoutCache.push(cachedItem);
    }

    switch (type) {
        case 'hidden':
        case 'text':
        case 'number':
        case 'number-select':
        case 'currency':
        case 'date':
        case 'date-single-format':
        case 'radio':
        case 'checkbox':
        case 'list':
        case 'assumption-list':
        case 'vehicle-find':
        case 'vehicle-summary':
        case 'collection-summary':
            item.value = cachedItem.observable;
            break;
    }

    if (options !== undefined) {
        item.options = options;

        if (options.assumptions !== undefined) {

            options.assumptions.forEach(assumptionId => {

                let cachedAssumptionItem = layoutCache.find(element => element.id === assumptionId);

                if (cachedAssumptionItem === undefined) {
                    cachedAssumptionItem = {
                        id: assumptionId,
                        observable: ko.observable()
                    };
                    layoutCache.push(cachedAssumptionItem);
                }
            });
        }
    }

    if (intro !== undefined) {
        item.intro = intro;
    }

    if (visibility !== undefined) {
        item.visibility = visibility;
    }

    let returnId = key;

    if (groupId !== undefined) {
        item.id(groupId + key);
        item.groupId = groupId;
        item.groupKey = groupKey;
        item.saved = ko.observable(false);
        item.removed = ko.observable(false);
        item.key = groupKey + key;
        item.collection = collection;

        returnId = groupId + returnId;
    }

    if (validation !== undefined) {
        item.validation = validation;

        if (groupId === undefined) {
            validationIds.push(item.id());
        } else {
            if (selectedStep !== undefined) {
                selectedStep().validatableIds.push(item.id());
            }
        }
    }

    stateItems.push(item);

    if (collection !== undefined && groupId === undefined) {

        item.collection = collection;
        item.collection.showDataCapture = ko.observable();
        item.collection.addAnother = ko.observable(false);
        item.collection.latestGroupId = ko.observable();

        const collectionSummary = {
            id: ko.observable("collectionSummary-" + key),
            ordinal: ko.observable(index),
            key: "collectionSummary-" + key,
            type: "collection-summary",
            name: collection.options.summaryLabel,
            value: ko.pureComputed(() => {

                var returnVal = false;

                returnVal = stateItems().find(function (stateItem) {

                    // if (stateItem.groupKey !== undefined) {
                    //     debugger;
                    // }

                    return stateItem.groupKey !== undefined && stateItem.groupKey === item.key && stateItem.saved !== undefined && stateItem.saved() === true;
                });

                //debugger;

                return returnVal !== undefined;
            })
        };

        //debugger;
        collectionSummary.visibility = {
            all: [{
                fact: collectionSummary.id(),
                operator: 'notEqual',
                value: undefined
            }, {
                fact: collectionSummary.id(),
                operator: 'equal',
                value: true
            }]
        }

        stateItems.push(collectionSummary);

        if (item.items === undefined) {
            item.items = ko.observableArray();
        }

        //item.items.push({ id: collectionSummary.id() });
    }

    return [{
        id: returnId
    }];
};

export default traverseLayout;
