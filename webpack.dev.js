const path = require('path');
const common = require('./webpack.common');
const { merge } = require('webpack-merge')

const config = {
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devtool: 'inline-source-map',
    devServer: {
        static: {
            directory: path.join(__dirname, 'dist'),
        },
        compress: true,
        port: 8000,
        open: true,
        liveReload: true,
    },
};

module.exports = merge(common, config);
