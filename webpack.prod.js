const path = require('path');
const common = require('./webpack.common');
const { merge } = require('webpack-merge')

const config = {
    entry: './src/ko-ddc.module.js',
    mode: 'production',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.[contenthash].js'
    }
};

module.exports = merge(common, config);
